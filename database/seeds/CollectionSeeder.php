<?php

use App\Collection;
use App\Document;
use App\Series;
use Illuminate\Database\Seeder;

class CollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $collections = [
            //User1 collections
            [
                'name' => 'test_collection_1',
                'description' => 'test1',
                'owner_id' => '1',
            ],
            [
                'name' => 'test_collection_2',
                'description' => 'test2',
                'owner_id' => '1',
            ],

            //User8 Collections
            [
                'name' => 'test_collection_3',
                'description' => 'test3',
                'owner_id' => '7',
            ],
            [
                'name' => 'test_collection_4',
                'description' => 'test4',
                'owner_id' => '7',
            ],
        ];

        $series = [
            [
                'name' => 'test_series_1',
                'description' => 'test1',
                'collection_id' => '1',
            ],
            [
                'name' => 'test_series_2',
                'description' => 'test2',
                'collection_id' => '2',
            ],
            [
            'name' => 'test_series_3',
            'description' => 'test3',
            'collection_id' => '3',
            ],
            [
                'name' => 'test_series_4',
                'description' => 'test4',
                'collection_id' => '3',
            ],
            [
                'name' => 'test_series_5',
                'description' => 'test5',
                'collection_id' => '4',
            ]
        ];

        $documents = [
            [
                'title' => 'test_doc_1_series_1',
                'description' => 'test1',
                'series_id' => '1',
            ],
            [
                'title' => 'test_doc_2_series_1',
                'description' => 'test2',
                'series_id' => '1',
            ],
            [
                'title' => 'test_doc_1_series_2',
                'description' => 'test2',
                'series_id' => '2',
            ],
            [
                'title' => 'test_doc_2_series_2',
                'description' => 'test2',
                'series_id' => '2',
            ]
        ];


        foreach ($collections as $collection) {
            Collection::firstOrCreate($collection);
        }

        foreach ($series as $series) {
            Series::firstOrCreate($series);
        }

        foreach ($documents as $doc) {
            Document::firstOrCreate($doc);
        }
    }
}
