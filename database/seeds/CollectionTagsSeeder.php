<?php

use App\CollectionTag;
use Illuminate\Database\Seeder;

class CollectionTagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
            ['name' => 'Document Usage',
                'type' => "string",
                'category' => 'Document'],
            ['name' => 'Language',
                'type' => "string",
                'category' => 'Document'],
            ['name' => 'Date Sent',
                'type' => "timestamp",
                'category' => 'Document'],
            ['name' => 'Word Count',
                'type' => "integer",
                'category' => 'Document'],
            ['name' => 'Tag Options 1',
                'type' => "string",
                'category' => 'Document'],
            ['name' => 'Tag Options 2',
                'type' => "string",
                'category' => 'Document'],
            ['name' => 'Tag Options 3',
                'type' => "string",
                'category' => 'Document'],


            ['name' => 'Sender Name',
                'type' => "string",
                'category' => 'Sender'],
            ['name' => 'Sender Gender',
                'type' => "string",
                'category' => 'Sender'],


            ['name' => 'Recipient Ethnicity',
                'type' => "string",
                'category' => 'Recipient'],
            ['name' => 'Recipient Name',
                'type' => "string",
                'category' => 'Recipient'],
            ['name' => 'Intended to have a wider audience',
                'type' => "string",
                'category' => 'Recipient']
        ];

        foreach ($tags as $tag_data) {
            CollectionTag::firstOrCreate($tag_data);
        }
    }
}
