<?php

use App\DocumentTagComboOption;
use Illuminate\Database\Seeder;
use App\CollectionTag;

class ComboOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options = [
            'Tag Options 1' => [
                'Option 1',
                'Option 2',
                'Option 3'
            ],
            'Tag Options 2' => [
                'Option 1',
                'Option 2',
                'Option 3',
                'Option 4',
                'Option 5',
                'Option 6'
            ],
            'Tag Options 3' => [
                'Option 1',
                'Option 2',
                'Option 3',
                'Option 4'
            ],
        ];

        foreach ($options as $tag_name => $values) {
            $tag_id = CollectionTag::where('name', $tag_name)->first()->id;
            foreach ($values as $value) {
                DocumentTagComboOption::firstOrCreate([
                    'tag_id' => $tag_id,
                    'value' => $value
                ]);
            }
        }
    }

}
