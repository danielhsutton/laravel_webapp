<?php

use Illuminate\Database\Seeder;
use App\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'firstname' => 'Daniel',
                'lastname' => 'Sutton',
                'username' => 'vanguard',
                'email' => 'danielhsutton@gmail.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(10),
            ],
            [
                'firstname' => 'Max',
                'lastname' => 'Power',
                'username' => 'TestUser1',
                'email' => 'MaxPower@gmail.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(10),
            ],
            [
                'firstname' => 'Ben',
                'lastname' => 'Davis',
                'username' => 'TestUser2',
                'email' => 'BenDavis@gmail.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(10),
            ],
            [
                'firstname' => 'Edward',
                'lastname' => 'McCain',
                'username' => 'TestUser3',
                'email' => 'EdwardMcCain@gmail.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(10),
            ],
            [
                'firstname' => 'Linda',
                'lastname' => 'Johnson',
                'username' => 'TestUser4',
                'email' => 'LindaJohnson@gmail.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(10),
            ],
            [
                'firstname' => 'Brain',
                'lastname' => 'Marshall',
                'username' => 'TestUser5',
                'email' => 'BrianMarshall@gmail.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(10),
            ],
            [
                'firstname' => 'Billy Bob',
                'lastname' => 'Thornton',
                'username' => 'TestUser6',
                'email' => 'BillyBob@gmail.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(10),
            ]
        ];

        foreach ($users as $user) {
            User::firstOrCreate($user);
        }

    }
}
