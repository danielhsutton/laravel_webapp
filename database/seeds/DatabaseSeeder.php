<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* UsersTableSeeder should only be used manually with:
         *
         *   php artisan db:seed --class=UsersTableSeeder
         *
         * Otherwise, these users with known passwords can be a security risk. */

        $this->call(UsersTableSeeder::class);
        $this->call(CollectionSeeder::class);
        $this->call(CollectionTagsSeeder::class);
        $this->call(ComboOptionsTableSeeder::class);
    }
}
