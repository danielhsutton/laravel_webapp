<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePermissionsColumnInCollectionPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->changeValidPermissions(['create', 'view', 'update', 'delete', 'anonymised', 'manage']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->changeValidPermissions(['create', 'view', 'update', 'delete', 'anonymised']);
    }

    private function changeValidPermissions($options)
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('collection_permissions', function ($table) {
            $table->string('permissions', 50)->change();
        });

        Schema::table('collection_permissions', function (Blueprint $table) {
            $table->dropForeign('collection_permissions_collection_id_foreign');
            $table->dropForeign('collection_permissions_owner_id_foreign');
            $table->dropUnique('unique_columns');
        });

        Schema::table('collection_permissions', function ($table) {
            $table->renameColumn('permissions', 'permissions_old');
        });
        Schema::table('collection_permissions', function ($table) use ($options) {
            $table->enum('permissions', $options)->default('view');
        });

        $all = DB::table('collection_permissions')->get();
        foreach ($all as $row) {
            if (!array_has($options, $row->permissions)) {
                DB::table('collection_permissions')->where('id', $row->id)->delete();
            }
        }


        Schema::table('collection_permissions', function ($table) {
            $table->dropColumn('permissions_old');
        });
        Schema::table('collection_permissions', function (Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('collection_id')->references('id')->on('collections')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unique(['owner_id', 'collection_id', 'permissions'], 'unique_columns');
        });
    }
}