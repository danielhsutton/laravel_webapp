<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 190)->unique();
            $table->string('anon_title', 190)->unique()->nullable();
            $table->string('description');
            $table->string('URL_page')->nullable();
            $table->date('URL_access_date')->nullable();
            $table->binary('raw_doc')->nullable();
            $table->integer('series_id')->unsigned();
            $table->integer('doc_type_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}