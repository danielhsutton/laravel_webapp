<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('collection_tags', function(Blueprint $table) {
            $table->foreign('collection_id')->references('id')->on('collections')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('owner_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('document_tags', function(Blueprint $table) {
            $table->foreign('document_id')->references('id')->on('documents')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('tag_id')->references('id')->on('collection_tags')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('owner_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('document_tags_combo_options', function(Blueprint $table) {
            $table->foreign('tag_id')->references('id')->on('collection_tags')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collection_tags', function (Blueprint $table) {
            $table->dropForeign('collection_tags_collection_id_foreign');
            $table->dropForeign('collection_tags_owner_id_foreign');
        });
        Schema::table('document_tags', function (Blueprint $table) {
            $table->dropForeign('document_tags_document_id_foreign');
            $table->dropForeign('document_tags_tag_id_foreign');
            $table->dropForeign('document_tags_owner_id_foreign');
        });
        Schema::table('document_tags_combo_options', function (Blueprint $table) {
            $table->dropForeign('document_tags_combo_options_tag_id_foreign');
        });
    }
}