<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\CollectionTag;

class CreateCollectionTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('collection_id')->unsigned()->nullable();
            $table->integer('owner_id')->unsigned()->nullable();
            $table->string('name');
            $table->enum('type', ['string', 'integer', 'timestamp']);
            $table->enum('category', ['Document', 'Sender', 'Recipient']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_tags');
    }
}