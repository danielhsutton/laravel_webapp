<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('collections', function(Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('series', function(Blueprint $table) {
            $table->foreign('collection_id')->references('id')->on('collections')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('documents', function(Blueprint $table) {
            $table->foreign('series_id')->references('id')->on('series')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('doc_type_id')->references('id')->on('doctypes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('collection_permissions', function(Blueprint $table){
            $table->foreign('owner_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('collection_id')->references('id')->on('collections')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


        Schema::enableForeignKeyConstraints();
    }

    /**
     * Laravel uniquely names the foreign key reference like this:
     *
     * <table_name>_<column_name>_foreign
     */
    public function down()
    {


        Schema::table('collections', function(Blueprint $table) {
            $table->dropForeign('collections_owner_id_foreign');
            $table->dropColumn('owner_id');
        });
        Schema::table('series', function(Blueprint $table) {
            $table->dropForeign('series_collection_id_foreign');
        });
        Schema::table('documents', function(Blueprint $table) {
            $table->dropForeign('documents_series_id_foreign');
            $table->dropForeign('documents_doc_type_id_foreign');
        });
        Schema::table('collection_permissions', function(Blueprint $table) {
            $table->dropForeign('collection_permissions_owner_id_foreign');
            $table->dropForeign('collection_permissions_collection_id_foreign');
        });
    }
}