<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTypeColumnInCollectionTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->changeTypes(['string', 'integer', 'timestamp', 'combo']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->changeTypes(['string', 'integer', 'timestamp']);
    }

    private function changeTypes($options)
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('collection_tags', function ($table) {
            $table->string('type', 50)->change();
        });

        Schema::table('collection_tags', function (Blueprint $table) {
            $table->dropForeign('collection_tags_collection_id_foreign');
            $table->dropForeign('collection_tags_owner_id_foreign');
        });

        Schema::table('collection_tags', function ($table) {
            $table->renameColumn('type', 'type_old');
        });
        Schema::table('collection_tags', function ($table) use ($options) {
            $table->enum('type', $options)->default('string')->after('name');
        });

        $all = DB::table('collection_tags')->get();
        foreach ($all as $row) {
            DB::table('collection_tags')->where('id', $row->id)->update(['type' => $row->type_old]);
        }

        Schema::table('collection_tags', function ($table) {
            $table->dropColumn('type_old');
        });

        Schema::table('collection_tags', function(Blueprint $table) {
            $table->foreign('collection_id')->references('id')->on('collections')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('owner_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }
}