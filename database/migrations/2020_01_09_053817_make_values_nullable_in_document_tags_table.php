<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeValuesNullableInDocumentTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_tags', function (Blueprint $table) {
            $table->string('text_value')->nullable()->change();
            $table->integer('int_value')->nullable()->change();
            $table->time('tstamp_value')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_tags', function (Blueprint $table) {
            $table->string('text_value')->change();
            $table->integer('int_value')->change();
            $table->time('tstamp_value')->change();
        });
    }
}