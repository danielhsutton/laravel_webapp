<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRawDocColumnInDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (DB::connection()->getDriverName() == "mysql") {
            DB::statement('ALTER TABLE documents MODIFY COLUMN raw_doc LONGBLOB');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (DB::connection()->getDriverName() == "mysql") {
            DB::statement('ALTER TABLE documents MODIFY COLUMN raw_doc BLOB');
        }
    }
}
