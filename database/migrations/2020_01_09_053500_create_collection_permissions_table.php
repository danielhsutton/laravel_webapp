<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionPermissionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection_permissions', function (Blueprint $table){
            $table->increments('id');
            $table->integer('owner_id')->unsigned();
            $table->integer('collection_id')->unsigned();
            $table->enum('permissions', ['create', 'view', 'update', 'delete', 'anonymised']);
            $table->unique(['owner_id', 'collection_id', 'permissions'], 'unique_columns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_permissions');
    }

}