<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUniqueDocumentIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_tags', function (Blueprint $table) {
            $table->unique(['document_id', 'tag_id', 'owner_id'], 'unique_doc_tag_owner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('document_tags', function($table) {
            $table->index('document_id', 'document_id_foreign');
        });

        Schema::table('document_tags', function($table) {
            $table->dropUnique('unique_doc_tag_owner');
        });

        Schema::enableForeignKeyConstraints();
    }
}