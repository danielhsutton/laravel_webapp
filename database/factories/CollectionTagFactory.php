<?php

use Faker\Generator as Faker;
use function factoryHelperFunctions\createOrFindObj;

$factory->define(App\CollectionTag::class, function (Faker $faker, array $array = null) {
    return [
        'owner_id' => createOrFindObj($array, 'owner_id', "User"),
        'collection_id' => createOrFindObj($array, 'collection_id', "Collection"),
        'name' => $faker->sentence(2),
        'type' => $faker->randomElement(['integer' ,'timestamp', 'string']),
        'category' => $faker->randomElement(['Document' ,'Sender', 'Recipient']),
    ];
});
