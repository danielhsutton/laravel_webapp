<?php

use Faker\Generator as Faker;
use function factoryHelperFunctions\createOrFindObj;

$factory->define(App\Document::class, function (Faker $faker, array $array = null) {
    return [
        'title' => $faker->name,
        'anon_title' => $faker->sentence(3),
        'description' => $faker->sentence(3),
        'series_id' => createOrFindObj($array, 'series_id', "Series"),
        'created_at' => $faker->dateTime($max = 'now'),
        'updated_at' => $faker->dateTime($max = 'now'),
    ];
});