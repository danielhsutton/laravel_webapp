<?php

use Faker\Generator as Faker;
use function factoryHelperFunctions\createOrFindObj;

$factory->define(App\Series::class, function (Faker $faker, array $array = null) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence(3),
        'collection_id' => createOrFindObj($array, 'collection_id', "Collection"),
        'created_at' => $faker->dateTime($max = 'now'),
        'updated_at' => $faker->dateTime($max = 'now'),
    ];
});
