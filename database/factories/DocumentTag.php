<?php

use Faker\Generator as Faker;
use function factoryHelperFunctions\createOrFindObj;

$factory->define(App\DocumentTag::class, function (Faker $faker, array $array = null) {
    return [
        'document_id' => createOrFindObj($array, 'document_id', "Document"),
        'tag_id' => createOrFindObj($array, 'tag_id', "CollectionTag"),
        'owner_id' => createOrFindObj($array, 'owner_id', "User"),
    ];
});
