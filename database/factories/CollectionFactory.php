<?php

use Faker\Generator as Faker;
use function factoryHelperFunctions\createOrFindObj;

$factory->define(App\Collection::class, function (Faker $faker, array $array = null) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence(3),
        'owner_id' => createOrFindObj($array, 'owner_id', "User"),
        'created_at' => $faker->dateTime($max = 'now'),
        'updated_at' => $faker->dateTime($max = 'now'),
    ];
});