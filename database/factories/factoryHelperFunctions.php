<?php
/**
 * User: Sutton
 * Date: 14/02/19
 * Time: 13:38
 */
namespace factoryHelperFunctions;

use Illuminate\Database\Eloquent\ModelNotFoundException;

if (!function_exists('factoryHelperFunctions\createOrFindObj'))   {
    function createOrFindObj($array, $key, $model)
    {
        $model = "App\\$model";

        if (isset($array[$key])) {
            try {
                return $model::findOrFail($array[$key])->id;
            } catch (ModelNotFoundException $ex) {
                return factory($model)->create()->id;
            }
        } else {
            return factory($model)->create()->id;
        }
    }
}


