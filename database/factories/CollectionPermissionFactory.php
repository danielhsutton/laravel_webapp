<?php

use Faker\Generator as Faker;
use function factoryHelperFunctions\createOrFindObj;

$factory->define(App\CollectionPermission::class, function (Faker $faker, array $array = null) {
    return [
        'owner_id' => createOrFindObj($array, 'owner_id', "User"),
        'collection_id' => createOrFindObj($array, 'collection_id', "Collection"),
        'permissions' => $faker->randomElement(['view' ,'create', 'update', 'delete', 'anonymised']),
    ];
});
