<?php

use Faker\Generator as Faker;
use function factoryHelperFunctions\createOrFindObj;

$factory->define(App\ComboBoxOption::class, function (Faker $faker, array $array = null) {
    return [
        'tag_id' => createOrFindObj($array, 'tag_id', "CollectionTag"),
        'value' => $faker->sentence(1),
        'created_at' => $faker->dateTime($max = 'now'),
        'updated_at' => $faker->dateTime($max = 'now'),
    ];
});