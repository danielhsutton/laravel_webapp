/*
    this function is used to show a hidden item on a html form.
 */
$(function () {
    $(window).ready(function () {
        //for each item with class selectBox
        $(".selectBox").each(function (index, htmlSB) {
            //htmlSB is the form item itself, the jQuery wrapper allows us to see more attributes
            var sb = $(htmlSB);
            //assign var prev to be the index of the current selected option in select box
            sb.data('prev', htmlSB.selectedIndex);

            //assign var for indexes and options values
            var currentValue = sb.data('prev');
            var currentOption = htmlSB.options[currentValue];

            if(currentOption.value == "Other"){

                //if the new option has attribute date-set, unhide the hidable item
                if (currentOption.dataset.show) {
                    document.getElementById(currentOption.dataset.show).style = '';
                }
            }

            sb.change(function () {
                //assign var for indexes and options values
                var oldValue = sb.data('prev');
                var newValue = htmlSB.selectedIndex;
                var oldOption = htmlSB.options[oldValue];
                var newOption = htmlSB.options[newValue];

                //if the old option has attribute date-set, hide the hidable item
                if (oldOption.dataset.show) {
                    document.getElementById(oldOption.dataset.show).style = 'display: none';

                }
                //if the new option has attribute date-set, unhide the hidable item
                if (newOption.dataset.show) {
                    document.getElementById(newOption.dataset.show).style = '';

                }

                //set var prev as the newly selected options index
                sb.data('prev', newValue);
            });
        });
    });

});


/*
This will find the id=added and check if it is clicked. If it is clicked then it will append a input field on
the id=options. This will also remove the last field entered when id=removed is clicked but stops removing once
it is less than 2 options.
 */
$(function() {
    $("#added").click(function(e) {
        e.preventDefault();
        $("#options").append("<input type='text' name='options[]' class='form-control' placeholder='Enter an option'>");

    });
    $("#removed").click(function (e) {
        e.preventDefault();
        if ($("#options input").length > 2) {
            $("#options").children().last().remove();
        }
    });
});



$(function () {
    $("#collection").change(function () {
        var newValue = this.selectedIndex;
        var newOption = this.options[newValue].value;
        $(".customTag").css("display", "none");
        $("#" + newOption).css("display", "inline");
    });
});



