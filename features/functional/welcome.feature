Feature: Login
  Background:
    Given a user called "Daniel" exists
    And a user called "John" exists


  Scenario: Log in as Daniel
    Given I am logged in as "Daniel"
    And I visit the path "/collections"
    Then I should see the text "Collections"

  Scenario: Log in as John
    Given I am logged in as "John"
    And I visit the path "/collections"
    Then I should see the text "Collections"
