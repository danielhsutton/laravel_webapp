<?php

namespace App\Http\Controllers;

use App\CollectionTag;
use App\ComboBoxOption;
use App\Document;
use App\DocumentTag;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TagController extends Controller
{
    /**
     * Show the list of Collection Tags the user has already created
     *
     * @param
     * @param
     * @throws
     * @return
     */
    public function viewTags($document_id, $user_id)
    {
        //retreives the document from DB with given id
        $document = Document::findOrFail($document_id);
        $user = User::findOrFail($user_id);
        //check if user can view tag
        $this->authorize('view', $document);


        $collection = $document->getCollection();

        $userDefinedCollectionTags = CollectionTag::getUserDefinedTags($user->id, $collection->id);

        $categories = $userDefinedCollectionTags->unique('category');

        $comboOptions = ComboBoxOption::where('collection_id', $collection->id)->where('owner_id', $user->id);

        return view('/tags/view', array('document' => $document, 'userDefinedCollectionTags' => $userDefinedCollectionTags,
            'comboOptions' => $comboOptions, 'categories' => $categories,'user'=> $user->id));
    }

    /**
     * Displays a view form to allow the user to create a custom Collection Tag
     *
     * @param
     * @return
     */
    public function createNewTag($id)
    {

        // Find document
        $document = Document::find($id);

        // Get the types
        $types = CollectionTag::getTypes();

        // JS requires "combo", but combo options are done in the DB
        // as 'string' types with ComboBoxOption entries
        $types[] = 'combo';

        return view('/tags/create', ['document' => $document, 'types' => $types]);

    }

    /**
     * Thit function stores a custom Collection tag created by the user in the DB.
     * The stored tag will be available to users when assigning doucment tags to a document
     *
     * @param
     * @return
     */
    public function store(Request $request)
    {
        //find document
        $document = Document::findOrFail($request->input('document_id'));
        $collection = $document->getCollection();

        $user = User::findOrFail($request->input('user_id'));

        if ($request->input('type') == "combo") {
            $this->validate($request, [
                'name' => 'required|unique:collection_tags,name',
                'type' => 'required',
                'category' => 'required',
                'options.*' => 'required|unique:document_tags_combo_options,value'
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required|unique:collection_tags,name',
                'type' => 'required',
                'category' => 'required',
            ]);
        }

        //create array of data for collection tag
        $col_tag_data = [
            'collection_id' => $collection->id,
            'owner_id' => $user->id,
            'name' => $request->input('name'),
            'type' => $request->input('type'),
            'category' => $request->input('category')
        ];

        if ($request->input('type') == "combo") {

            $col_tag_data['type'] = "string";
        }

        //create a collection tag using the array
        //also get collectionTag obj
        $collection_tag = CollectionTag::create($col_tag_data);

        if ($request->input('type') == "combo") {

            foreach ($request->input('options') as $comboOption) {

                $combo_option_data = [
                    'tag_id' => $collection_tag->id,
                    'value' => $comboOption,
                ];

                //create a combo option row using the POST data
                ComboBoxOption::create($combo_option_data);

            }
        }

        return redirect()->route('add-tags', ['document' => $document->id, 'user_id' => $user->id]);
    }

    /**
     * Displays a view form to allow the user to edit a custom Collection Tag
     *
     * @param
     * @param
     * @param
     * @throws
     * @return
     *
     */
    public function editTags(Request $request, $document_id, $user_id, $tag_id)
    {
        //find document
        //retreives the document from DB with given id
        $document = Document::findOrFail($document_id);
        $user = User::findOrFail($user_id);
        //check if user can view tag
        $this->authorize('update', $document);

        // Get the types
        $types = CollectionTag::getTypes();

        // JS requires "combo", but combo options are done in the DB
        // as 'string' types with ComboBoxOption entries
        $types[] = 'combo';

        $collection = $document->getCollection();

        $userDefinedCollectionTags = CollectionTag::getUserDefinedTags($user->id, $collection->id);

        $tag = $userDefinedCollectionTags->where('id', '=', $tag_id)->first();

        $comboOptions = ComboBoxOption::where('collection_id', $collection->id)->where('owner_id', $user->id);

        return view('/tags/edit', array('document' => $document, 'tag' => $tag,
            'comboOptions' => $comboOptions,  'types' => $types));

    }


    /**
     * Edits the current Collection Tag defined by the user and updates in database
     *
     * @param
     * @param
     * @return
     */
    public function update(Request $request, $tag_id)
    {
        //find document
        $document = Document::findOrFail($request->input('document_id'));
        $collection = $document->getCollection();
        $tag = CollectionTag::findOrFail($tag_id);
        $user = User::findOrFail($request->input('user_id'));

//        $combo_options = ComboBoxOption::where("tag_id", $tag_id)->get();


        if ($request->input('type') == "combo") {
            $this->validate($request, [
                'name' => 'required',
                'options.*' => 'required|unique:document_tags_combo_options,value'
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required',
            ]);
        }


//        if ($request->input('type') == "combo") {
//
//            foreach ($combo_options as $combo_option){
//
//                $combo_option->delete();
//
//            }
//
//            foreach ($request->input('options') as $comboOption) {
//
//                $data = [
//                    'tag_id' => $tag_id,
//                    'value' => $comboOption,
//
//                ];
//                ComboBoxOption::create($data);
//
//            }
//        }

        $tag->name = Input::get('name');

//        $tag->type = Input::get('type');
//        if(Input::get('type') == "combo"){
//
//            $tag->type = "string";
//
//        }
//        $tag->category = Input::get('category');

        $tag->save();

        return redirect()->route('view-tags', ['document' => $document->id, 'user_id' => $user->id]);
    }

    /**
     * Deletes the Collection Tag in the table collection_tags with the id = tag_id
     *
     * @param
     * @param
     * @throws
     * @return
     */
    public function destroy (Request $request, $tag_id){
        $document = Document::findOrFail($request->input('document_id'));
        $tag = CollectionTag::findOrFail($tag_id);
        $user = User::findOrFail($request->input('user_id'));

        //check if user can delete tag
        $this->authorize('delete', $document);

        $tag->delete();

        return redirect()->route('view-tags', ['document' => $document->id, 'user_id' => $user->id]);

    }


    /**
     * Show a form with a list of Collection Tags available to the user.
     * The user can assign new values to these tags for this document and store them in docuemnt_tags.
     * The user can also change the current values of the tags if they have one and update the correct
     * row in document_tags instead
     *
     * @param
     * @param
     * @param
     * @throws
     * @return
     */
    public function addTags(Request $request, $document_id, $user_id)
    {
        $document = Document::findOrFail($document_id);
        $user = User::findOrFail($user_id);

        //check if user can create document
        $this->authorize('update', $document);

        $collection = $document->getCollection();

        $userCollectionTags = CollectionTag::getUserTags($user->id, $collection->id);

        $categories = $userCollectionTags->unique('category');

        $comboOptions = ComboBoxOption::all();

        return view('/document/tagging', array('document' => $document, 'tags' => $userCollectionTags, 'comboOptions' => $comboOptions, 'categories' => $categories));
    }

    /**
     * Saves the current values assigned to each Collection Tag to the current document as Document Tags
     * in docuemnt_tags table
     *
     * The user can assign new values to these tags for this document and store them in docuemnt_tags.
     * The user can also change the current values of the tags if they have one and update the correct
     * row in document_tags instead
     *
     * form_data is an array of the values sent in the form with a structure seen below where the integer in
     * the key represents the corresponding CollectionTags id.
     * ValueN represents the default inputs in the form (text, integer, date/time, select box) while the
     * valueNother is a special case for the select boxes in the form.
     * Each select box in the form has a value "Other" at the bottom. When the user selects this value,
     * a hidden text input appears, these are named as valueNother and their values are the text the user inputs.
     *
     * form_data{
     *              "value1"        =>      'Unknown',                  <---  1)
     *              "value1other'   =>      null,
     *              "value2"        =>      "[not set]",                <---  2)
     *              "value2other"   =>      "Device",
     *              "value3"        =>      1234,
     *              "value4"        =>      "2015-02-02 19:03:00"
     *              "value5"        =>      "Other",                    <---  3)
     *              "value5other"   =>      "other_value"
     *          };
     *
     * Other value cases:
     *  1) if valueN is set but valueNother is null, save the value for key valueN as the user has not selected "Other" and does not want to
     *      specify an other value
     * 2) if valueN is set and valueNother IS NOT null, but the value for key valueN is NOT "Other", save the value for key valueN, as the
     *      user selected "Other" and input a specific value, but then changed their mind and selected another value in the selectbox
     * 3) if valueN is set and valueNother IS NOT null, and the value for key valueN IS "Other", save the value for key valueNother, as
     *      the user has specified a value and wishes to save it.
     *
     * @param
     * @param
     * @param
     * @throws
     * @return
     */
    public function storeTags($doc_id, $user_id, Request $request)
    {
        //find document and user
        $document = Document::findOrFail($doc_id);
        $user = User::findOrFail($user_id);

        //check if user can edit document
        $this->authorize('update', $document);

        // map from tag id to document tag in database
        $document_tags = DocumentTag::where('document_id', $document->id)->get();
        $document_tagid_to_tag = [];
        foreach ($document_tags as $document_tag) {
            $document_tagid_to_tag[$document_tag->tag_id] = $document_tag;
        }

        // map from tag id to form tag
        $table_form_tagid_to_value = [];
        foreach ($request->input('tag_ids') as $tag_id) {
            $table_form_tagid_to_value[$tag_id] = [];
        }

        // array of values sent from the form
        $form_data = $request->input('form_data');

        foreach ($form_data as $key => $tag_value) {
            $matches = [];

            //if pattern matches value1, value2... valueN not value1other, value2other... valueNother
            if (preg_match("/^[a-z]+(\d+)$/", $key, $matches)) {
                $tag_id = $matches[1];

                /** @var CollectionTag $collection_tag */
                $collection_tag = CollectionTag::findOrFail($tag_id);
                
                // if there is a value and is not null
                if ($tag_value) {

                    //prepare data to be inserted
                    $data = [
                        'document_id' => $document->id,
                        'tag_id' => (int) $tag_id,
                        'owner_id' => $user->id
                    ];

                    if ($collection_tag->isString()) {
                        // if the collection tag is a string, set the tag value to be inserted into the table under column 'text_value'
                        $data['text_value'] = $tag_value;

                        //if the collection tag is a select box with multiple values it must check what the tag_value is
                        if ($collection_tag->isCombo()) {
                            //concatenate key with "other" to create value1other, value2other... valueNother
                            $other_key = $key . "other";
                            //if the for_data has the other_key valueNother, and if the value of other_key is not null, and the corresponding key valueN is "Other"
                            if (array_has($form_data, $other_key) && $form_data[$other_key] && $form_data[$key] == 'Other') {
                                $data['text_value'] = $form_data[$other_key];
                            }
                        }
                    } elseif ($collection_tag->isInteger()) {
                        $data['int_value'] = (int) $tag_value;
                    } elseif ($collection_tag->isTimestamp()) {
                        $datetime = preg_split ( "/[\:T\-]/" , $tag_value);
                        if(strlen($datetime[0]) == 4){
                            $data['tstamp_value'] = $tag_value;
                        }else{
                            throw new \InvalidArgumentException("date is invalid");
                        }
                    } else {
                        throw new \InvalidArgumentException("Tag is of an unknown type");
                    }

                    $table_form_tagid_to_value[$tag_id] = $data;
                }
            }
        }

        //remove $table_form_tagid_to_value indexes with empty values
        foreach ($table_form_tagid_to_value as $tag_id => $value_tag) {
            if(!$value_tag){
                unset($table_form_tagid_to_value[$tag_id]);
            }
        }

        //for each document tag, if value from form does not exist, delete it from document tag table
        foreach ($document_tagid_to_tag as $tag_id => $document_tag) {
            if (!array_has($table_form_tagid_to_value, $tag_id)) {
                $document_tag->delete();
            }
        }

        //for each tag in form data
        foreach ($table_form_tagid_to_value as $tag_id => $document_tag_data) {

            //if tag from form data is not in document tag table
            if (!array_has($document_tagid_to_tag, $tag_id)) {

                //and data is not empty
                if(!empty($document_tag_data)){
                    // create document tag
                    DocumentTag::create($document_tag_data);
                }
            } else { //else if tag from form data IS in document tag table, it should be replaced
                $document_tagid_to_tag[$tag_id]->update($document_tag_data);
            }
        }
        return redirect()->route('documents.show', [$document]);
    }
}
