<?php

namespace App\Http\Controllers;

use App\Collection;
use App\Series;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


/**
 * Class CollectionController.
 *
 * This class controls the CRUD of collections from a relation database
 *
 * @author Daniel Sutton
 */
class CollectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //this is so that users must be logged in to view collections
        $this->middleware('auth');
    }

    /**
     * Display a list of the collections.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get collections from db
        $collections = Collection::all();

        // load the view and pass an array of collections
        return view('collection/index', array('collections' => $collections));
    }

    /**
     * Show a create form for creating a collection with a name and desc.
     *
     * @return
     */
    public function create()
    {
        //check if user can create collection
        $this->authorize('create', Collection::class);

        return view('collection/create');
    }

    /**
     * Store a collection in database by inserting given form data from Request.
     *
     * @param Request $request
     *      The POST request containing the data from the collection create form
     * @return collections, URL /collections, route(collections.index), calls index() view of all collections
     */
    public function store(Request $request)
    {
        //check if user can create collection
        $this->authorize('create', Collection::class);

        $this->validate($request, ['name' => 'required|unique:collections',
            'description' => 'required',
        ]);

        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'owner_id' => $request->user()->id,
        ];

        //create a collection using the POST data
        Collection::create($data);

        return redirect()->route('collections.index');
    }

    /**
     * Display a specific collection from a query from the database.
     *
     * @param  int $id
     *      The id of the given collection to be retrieved from the collections table in database
     * @return The view of the chosen collection
     */
    public function show($id)
    {
        //retrieves the collection from DB with given id
        try {
            $collection = Collection::findOrFail($id);
        } catch (ModelNotFoundException $ex) {
            $msg = "Collection not found";
            return view('errors/404', array('message' => $msg));
        }

        //check if user can view collection
        $this->authorize('view', $collection);

        //find all series that belong to the collection
        $series = Series::all()->where('collection_id', $collection->id);

        //array contains a single collection from the DB with all attributes {name, description...}
        //This info will be displayed in view
        return view('collection/view', array('collection'=>$collection, 'series'=>$series));
    }

    /**
     * Show the collection and edit it.
     *
     * @param  int $id
     *      The id of the given collection to be retrieved from the collections table in database
     * @return The edit page with the chosen collection as an arguement
     */
    public function edit($id)
    {
        //retreives the collection from DB with given id
        $collection = Collection::find($id);

        //check if user can update collection
        $this->authorize('update', $collection);

        //array contains a single collection from the DB with all attributes {name, description...}
        //This info will used as placeholders in collection edit form
        return view('/collection/edit', array('collection' => $collection));

    }

    /**
     * Update a specific collection in the DB with information from the collection edit form
     *
     * @param  int $id
     *      The id of the given collection to be retrieved from the collections table in database and edited
     * @return The view page of the given collection with the collection as an argument using the updated information and
     */
    public function update($id, Request $request)
    {
        //find collection
        $collection = Collection::find($id);

        //check if user can update collection
        $this->authorize('update', $collection);

        //If validation passes, code will continue executing normally.
        //However, if validation fails, an  Illuminate\Contracts\Validation\ValidationException will be thrown
        //and is automatically caught and a redirect to the user's previous location.
        $this->validate($request, ['name' => 'required',
            'description' => 'required',
        ]);

        //update existing columns
        $collection->name = Input::get('name');
        $collection->description = Input::get('description');
        $collection->save();

        return redirect()->route('collections.show', ['collection' => $collection]);
    }

    /**
     * Delete a specific collection from the database.
     *
     * @param  int $id
     *      id of collection to remove
     * @return
     *      collections, URL /collections, route(collections.index), calls index() view of all collections
     */
    public function destroy($id)
    {
        //find collection
        $collection = Collection::find($id);

        //check if user can delete collection
        $this->authorize('delete', $collection);

        $collection->delete();

        return redirect()->route('collections.index');
    }

}

