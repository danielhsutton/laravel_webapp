<?php

namespace App\Http\Controllers;

use App\Collection;
use App\CollectionPermission;
use App\User;
use DB;
use Illuminate\Http\Request;

class CollectionPermissionController extends Controller
{
    /**
     * Display a list of the Users and corresponding permissions checkboxes for CRUD verbs + anon view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        // get collection and permissions from db
        $collection = Collection::find($id);
        $permissions = CollectionPermission::all()->where('collection_id', $collection->id);

        //get all users but collection owner, as they have full access by default and cannot be changed
        $users = User::whereNotIn('email', array($collection->owner()->first()->email))->get();

        // load the view and pass an array of collections
        return view('/collection/permissions', array('users' => $users,
                                                            'collection' => $collection, 'permissions'=>$permissions));
    }

    /**
     * Update the permissions in database, collection_permissions table. The POST request contains a list of arrays
     * for each CRUD verb + anon view. Each array is compared to the database table. If the permission is in the database
     * but not in the form, it gets deleted from the database table. If the permission is not in the database, yet is in
     * the form, the permission gets added. Otherwise everything stays the same.
     *
     * @param Request $request
     *      The POST request containing the data for the checkboxes from the permissions form
     * @param $id
     *      The id of a collection to edit its permissions
     * @return \Illuminate\Http\RedirectResponse
     *      redirects to CollectionController::show, the view of the collection with a list of series
     */
    public function update(Request $request, $id)
    {
        //the collection for the permissions
        $collection = Collection::find($id);

        //the permission constants for CRUDing + anon view
        $permissionverbs = CollectionPermission::permissions;

        //array of permissions for this collection
        $permissions = CollectionPermission::all()->where('collection_id', $collection->id)->sortBy('owner_id');


        //creating 2d array of userid mapping to permissions
        // Map<String(owner_id), Map<String(permission_verb), CollectionPermission>>
        $table_userid_to_permission = [];
        foreach ($permissions as $permission) {
            $table_userid_to_permission[$permission->owner_id] = [];
        }
        foreach ($permissions as $permission) {
            $table_userid_to_permission[$permission->owner_id][$permission->permissions] = $permission;
        }

        //create 2d array of userids mapping to permissions verbs
        //Map<String(user_id), List<String(permission verbs>>
        $table_form_userid_to_permissionverb = [];
        foreach ($permissionverbs as $permissionverb) {
            $userids = $request->input($permissionverb);

            //if the array exists, ie if 1 check box has been checked
            if ($userids) {
                foreach ($userids as $userid) {

                    //if the userid key has not been created, create it
                    if (!isset($table_form_userid_to_permissionverb[$userid])) {
                        $table_form_userid_to_permissionverb[$userid] = [];
                    }
                    //add permission as value to userid key
                    $table_form_userid_to_permissionverb[$userid][] = $permissionverb;
                }
            }
        }

        // Step 1. Go through the permissions, delete those that are not in the form
        foreach($permissions as $permission) {
            $inForm = isset($table_form_userid_to_permissionverb[$permission->owner_id])
                && in_array($permission->permissions, $table_form_userid_to_permissionverb[$permission->owner_id]);
            if (!$inForm) {
                $permission->delete();
            }
        }
        // Step 2. Go through the form, add those that are not in the DB
        foreach ($table_form_userid_to_permissionverb as $uid => $form_permissionverbs) {

            foreach ($form_permissionverbs as $form_permissionverb) {

                $inDB = isset($table_userid_to_permission[$uid][$form_permissionverb]);

                if (!$inDB) {
                    //create a collection permission using the POST data
                    CollectionPermission::create([
                        'owner_id' => $uid,
                        'collection_id' => $collection->id,
                        'permissions' => $form_permissionverb,
                    ]);
                }
            }
        }

        return redirect()->route('collections.show', $collection);
    }
}
