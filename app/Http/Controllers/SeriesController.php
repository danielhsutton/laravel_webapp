<?php

namespace App\Http\Controllers;

use App\Collection;
use App\Document;
use App\Series;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

/**
 * Class SeriesController.
 *
 * This class controls the CRUD of series from a relation database
 *
 * @author Daniel Sutton
 */
class SeriesController extends Controller
{

    /**
     * Show a create form for creating a series
     *
     * @param int $id
     *          The id of the collection that the series will belong to
     * @return view to create a series
     */
    public function create($id)
    {
        $collection = Collection::find($id);

        //check if user can create series
        $this->authorize('createSeries', $collection);

        return view('/series/create', array('collection'=>$collection));
    }

    /**
     * Store a series in database by inserting given form data from Request.
     *
     * @param Request $request
     *      The POST request containing the data from the series create form
     * @return series, URL /series, route(series.index), calls index() view of all series
     */
    public function store(Request $request)
    {
        $collection = Collection::find($request->input('collection_id'));

        //check if user can create series
        $this->authorize('createSeries', $collection);

        $this->validate($request, [
            'name' => 'required|unique:series',
            'description'=>'required',
        ]);

        //data that will be inserted into new row in series table
        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'collection_id' => $request->input('collection_id')
        ];

        //create a series using the POST data
        Series::create($data);


        //return $controller->show($data['collection_id']);
        return redirect()->route('collections.show' , ['collection' => $data['collection_id']]);
    }

    /**
     * Display a specific series from a query from the database.
     *
     * @param  int  $id
     *      The id of the given series to be retrieved from the series table in database
     * @return The view of the chosen series
     */
    public function show($id)
    {
        //retreives the series from DB with given id
        $series = Series::find($id);

        //check if user can view series
        $this->authorize('view', $series);

        $documents = Document::all()->where('series_id', $series->id);

        //array contains a single series from the DB with all attributes {name, description}
        //This info will be displayed in view
        return view('/series/view', array('series'=>$series, 'documents'=>$documents));
    }

    /**
     * Show the series and edit it.
     *
     * @param  int  $id
     *      The id of the given series to be retrieved from the series table in database
     * @return The edit page with the chosen series as an arguement
     */
    public function edit($id)
    {
        //retreives the series from DB with given id
        $series = Series::find($id);

        //check if user can edit series
        $this->authorize('update', $series);

        //array contains a single series from the DB with all attributes {name, description...}
        //This info will used as placeholders in series edit form
        return view('/series/edit', array('series'=>$series));
    }

    /**
     * Update a specific series in the DB with information from the series edit form
     *
     * @param  int  $id
     *      The id of the given series to be retrieved from the series table in database and edited
     * @return The view page of the given series with the series as an argument using the updated information and
     */
    public function update($id, Request $request)
    {
        //find series
        $series = Series::find($id);

        //check if user can edit series
        $this->authorize('update', $series);

        //If validation passes, code will continue executing normally.
        //However, if validation fails, an  Illuminate\Contracts\Validation\ValidationException will be thrown
        //and is automatically caught and a redirect to the user's previous location.
        $this->validate($request, ['name' => 'required|unique:series',
            'description'=>'required',
        ]);

        //update existing columns
        $series->name           = Input::get('name');
        $series->description    = Input::get('description');
        $series->save();

        return redirect()->route('series.show' , ['series' => $series]);

    }

    /**
     * Delete a specific series from the database.
     *
     * @param  int  $id
     *      id of series to remove
     * @return
     *      CollectionController::show, the view of the collection with a list of series
     */
    public function destroy($id)
    {
        //find series
        $series = Series::find($id);

        //check if user can delete series
        $this->authorize('delete', $series);

        //find collection associated w series
        $collection = $series->collection()->first();

        $series->delete();

        return redirect()->route('collections.show' , ['collection' => $collection]);
    }

}
