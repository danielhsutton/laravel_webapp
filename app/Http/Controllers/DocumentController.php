<?php

namespace App\Http\Controllers;

use App\Document;
use App\Series;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class DocumentController extends Controller
{
    /**
     * Show a create form for creating a document
     *
     * @return
     */
    public function create($id)
    {
        $series = Series::find($id);

        //check if user can create document
        $this->authorize('createDocument', $series);

        return view('/document/create', array('series' => $series));
    }

    /**
     * Store a document in database by inserting given form data from Request.
     *
     * @param Request $request
     *      The POST request containing the data from the document create form
     * @return SeriesController@show, returns view of series with list of all documents including newly created one
     */
    public function store(Request $request)
    {
        $series = Series::find($request->input('series_id'));

        //check if user can create document
        $this->authorize('createDocument', $series);

        $this->validate($request, [
            'title' => 'required|unique:documents',
            'description' => 'required',
            'document' => 'mimes:pdf',
        ]);

        $file = null;

        if ($request->file('document')) {
            $file = base64_encode(file_get_contents($request->file('document')));
        }


        //data that will be inserted into new row in document table
        $data = [
            'title' => $request->input('title'),
            'anon_title' => $request->input('anontitle'),
            'description' => $request->input('description'),
            'raw_doc' => $file,
            'URL_page' => $request->input('URL_page'),
            'URL_access_date' => $request->input('URL_access_date'),
            'series_id' => $request->input('series_id'),
            'doc_type_id' => $request->input('doc_type_id'),
        ];

        //create a document using the POST data
        Document::create($data);


        return redirect()->route('series.show', ['series' => $data['series_id']]);
    }

    /**
     * Display a specific document from a query from the database.
     *
     * @param  int $id
     *      The id of the given document to be retrieved from the document table in database
     * @return The view of the chosen document
     */
    public function show($id)
    {
        //retreives the document from DB with given id
        $document = Document::find($id);

        //check if user can view document
        $this->authorize('view', $document);

        //array contains a single document from the DB with all attributes {name, description...}
        //This info will be displayed in view
        return view('/document/view', array('document' => $document));
    }

    /**
     * Get the document ready for showing
     */
    public function getDocument($id)
    {
        //retreives the document from DB with given id
        $document = Document::find($id);

        //check if user can edit document
        $this->authorize('update', $document);

        return view('documents/{id}/pdf', array('document' => $document));
    }

    /**
     * Show the document and edit it.
     *
     * @param  int $id
     *      The id of the given document to be retrieved from the document table in database
     * @return The edit page with the chosen document as an argument
     */
    public function edit($id)
    {
        //retreives the document from DB with given id
        $document = Document::find($id);

        //check if user can edit document
        $this->authorize('update', $document);

        //array contains a single document from the DB with all attributes {name, description...}
        //This info will used as placeholders in document edit form
        return view('/document/edit', array('document' => $document));
    }

    /**
     * Update a specific document in the DB with information from the document edit form
     *
     * @param  int $id
     *      The id of the given document to be retrieved from the document table in database and edited
     * @return The view page of the given document with the document as an argument using the updated information and
     */
    public function update($id, Request $request)
    {
        //find document
        $document = Document::find($id);

        //check if user can edit document
        $this->authorize('update', $document);

        //If validation passes, code will continue executing normally.
        //However, if validation fails, an  Illuminate\Contracts\Validation\ValidationException will be thrown
        //and is automatically caught and a redirect to the user's previous location.
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'document' => 'mimes:pdf',
        ]);
        $file = null;
        if ($request->file('document')) {
            $file = base64_encode(file_get_contents($request->file('document')));
        }
        else{

            $file = $document->raw_doc;
        }
        //update existing columns
        $document->title = Input::get('title');
        $document->description = Input::get('description');
        $document->title = Input::get('title');
        $document->anon_title = Input::get('anontitle');
        $document->description = Input::get('description');
        $document->raw_doc = $file;
        $document->URL_page = Input::get('URL_page');
        $document->URL_access_date = Input::get('URL_access_date');
        $document->save();

        return redirect()->route('documents.show', ['documents' => $document['id']]);
    }

    /**
     * Delete a specific document from the database.
     *
     * @param  int $id
     *      id of document to remove
     * @return
     *      SeriesController::show, the view of the series with a list of documents
     */
    public function destroy($id)
    {
        //find document
        $document = Document::find($id);

        //check if user can delete document
        $this->authorize('delete', $document);

        //find collection associated w series
        $series = $document->series()->first();

        $document->delete();

        return redirect()->route('series.show', ['series' => $series]);

    }

    public function pdf($id)
    {
        //find document
        $document = Document::find($id);

        //check if user can view document
        $this->authorize('view', $document);

        $file_contents = base64_decode($document->raw_doc);

        return response($file_contents)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/x-pdf')
            ->header('Content-Length', strlen($file_contents))
            ->header('Content-Disposition', 'inline; filename="example.pdf"')
            ->header('Content-Transfer-Encoding', 'binary');
    }

    /**
     * Show a create form for creating a digitised text of the document
     *
     * @return
     */
    public function createDigitisedText($id)
    {
        $document = Document::find($id);

        //check if user can create document
        $this->authorize('update', $document);

        return view('/document/digitised', array('document' => $document));
    }


    /**
     * Update a specific document in the DB with the digitised text
     *
     * @param  int $id
     *      The id of the given document to be retrieved from the document table in database and edited
     * @return The view page of the given document with the document as an argument using the updated information and
     */
    public function storeDigitisedText($id, Request $request)
    {
        //find document
        $document = Document::find($id);

        //check if user can edit document
        $this->authorize('update', $document);

        //If validation passes, code will continue executing normally.
        //However, if validation fails, an  Illuminate\Contracts\Validation\ValidationException will be thrown
        //and is automatically caught and a redirect to the user's previous location.
        $this->validate($request, [
            'text' => 'required',
        ]);

        //update existing columns
        $document->digitised_text = Input::get('text');
        $document->save();

        return redirect()->route('documents.show', ['documents' => $document['id']]);
    }


}
