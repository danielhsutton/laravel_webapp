<?php

namespace App\Policies;

use App\User;
use App\Collection;
use App\CollectionPermission;
use Illuminate\Auth\Access\HandlesAuthorization;

class CollectionPolicy
{
    use HandlesAuthorization;

    /**
     * Checks whether a collection can be updated.
     * A user can update the collection if they own the collection, or
     * if they have the correct permissions to update the collection by
     * the owner.
     *
     * @param  \App\User $user
     * @param  \App\Collection $collection
     * @return true if the user owns the collection OR if the user has
     * a permission in the table CollectionPermissions in database ELSE false
     */
    public function update(User $user, Collection $collection)
    {
        //update permission for this collection by this user
        $userPermission = CollectionPermission::where('permissions', 'update')
            ->where('collection_id', $collection->id)
            ->where('owner_id', $user->id)
            ->first();

        return $user->id == $collection->owner_id || isset($userPermission);

    }

    /**
     * Checks whether a collection can be managed.
     * Return true if the user owns the collection or if the user has
     * a permission in the database
     *
     * @param  \App\User $user
     * @param  \App\Collection $collection
     * @return
     */
    public function manage(User $user, Collection $collection)
    {
        //update permission for this collection by this user
        $userPermission = CollectionPermission::where('permissions', 'manage')
            ->where('collection_id', $collection->id)
            ->where('owner_id', $user->id)
            ->first();

        return $user->id == $collection->owner_id || isset($userPermission);

    }

    /**
     * Checks whether a collection can be deleted.
     * A user can delete the collection if they own the collection, or
     * if they have the correct permissions to delete the collection by
     * the owner.
     *
     * @param  \App\User $user
     * @param  \App\Collection $collection
     * @return true if the user owns the collection OR if the user has
     * a permission in the table CollectionPermissions in database ELSE false
     */
    public function delete(User $user, Collection $collection)
    {
        //delete permission for this collection by this user
        $userPermission = CollectionPermission::where('permissions', 'delete')
            ->where('collection_id', $collection->id)
            ->where('owner_id', $user->id)
            ->first();

        return $user->id == $collection->owner_id || isset($userPermission);
    }


    /**
     * Check if the given user can create a collection.
     *
     * @param  \App\User $user
     * @return true
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Check if the user can view the collection.
     * A user can view the collection if they own the collection, or
     * if they have the correct permissions to view the collection by
     * the owner.
     *
     * @param  \App\User $user
     * @param  \App\Collection $collection
     * @return true if the user owns the collection OR if the user has
     * a permission in the table CollectionPermissions in database ELSE false
     */
    public function view(User $user, Collection $collection)
    {
        //view permission for this collection by this user
        $userPermission = CollectionPermission::where('permissions', 'view')
            ->where('collection_id', $collection->id)
            ->where('owner_id', $user->id)
            ->first();

        return $user->id == $collection->owner_id || isset($userPermission);
    }

    /**
     * Determine whether the user can create series.
     * A user can create a series in the collection if they have the
     * permission to create over that collection
     *
     * @param  \App\User  $user
     * @param  \App\Collection  $collection
     * @return true if the user owns the collection or if the user has
     * a permission in the database ELSE false
     */
    public function createSeries(User $user, Collection $collection)
    {
        //create permission for this collection by this user
        $userPermission = CollectionPermission::where('permissions', 'create')
            ->where('collection_id', $collection->id)
            ->where('owner_id', $user->id)
            ->first();

        return $user->id == $collection->owner_id || isset($userPermission);
    }
}

