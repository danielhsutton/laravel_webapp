<?php

namespace App\Policies;

use App\Collection;
use App\CollectionTag;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CollectionTagPolicy
{
    use HandlesAuthorization;

    /**
     * Checks if the user is the owner of the collection tag
     *
     * @param \App\User
     *          $user the user id to check if they are the owner of a tag
     * @param \App\CollectionTag
     *          $collectionTag the CollectionTag to check if the user is an owner of
     * @return true if the user is the owner of the tag
     */
    public function ownsTag(User $user, CollectionTag $collectionTag)
    {
        return $user->id == $collectionTag->id;
    }

    /**
     * Check if the user can use the collection tag.
     * A user can use the tag if it is a default tag or
     * if they own the tag.
     *
     * @param  \App\User $user
     * @param  \App\Collection $col_tag
     * @return true if the user owns the tag
     */
    public function use(User $user, CollectionTag $col_tag)
    {
        //todo: get permission of user to 'use' col tag in collectionTagPermisions tabele

        //if user owns the tag OR if collection tag is default (no owner_id and collection_id)
        return $user->id == $col_tag->owner_id || ($col_tag->owner_id == null && $col_tag->collection_id == null);
    }
}
