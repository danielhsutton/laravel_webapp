<?php

namespace App\Policies;

use App\User;
use App\Series;
use App\CollectionPermission;
use App\Collection;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Policies\CollectionPolicy;

class SeriesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the series using Eloquent relationship
     * found in app/Series.php. Checks if user can perform same verb action on collection
     *
     * @param  \App\User  $user
     * @param  \App\Series  $series
     * @return mixed
     */
    public function view(User $user, Series $series)
    {
        //retreives the collection from series relationship using foreign key collection_id
        $collection = $series->collection()->first();

        return $user->can('view', $collection);
    }


    /**
     * Determine whether the user can update the series using Eloquent relationship
     * found in app/Series.php. Checks if user can perform same verb action on collection
     *
     * @param  \App\User  $user
     * @param  \App\Series  $series
     * @return mixed
     */
    public function update(User $user, Series $series)
    {
        //retreives the collection from series relationship using foreign key collection_id
        $collection = $series->collection()->first();

        return $user->can('update', $collection);
    }

    /**
     * Determine whether the user can delete the series using Eloquent relationship
     * found in app/Series.php. Checks if user can perform same verb action on collection
     *
     * @param  \App\User  $user
     * @param  \App\Series  $series
     * @return mixed
     */
    public function delete(User $user, Series $series)
    {
        //retreives the collection from DB with given id
        $collection = $series->collection()->first();

        return $user->can('delete', $collection);
    }

    /**
     * Determine whether the user can create document.
     * Can create
     *
     * @param  \App\User  $user
     * @param  \App\Series  $series
     * @return mixed
     *         if user can create a series, it can create a document, return true, else false
     *
     */
    public function createDocument(User $user, Series $series)
    {
        return $user->can('createSeries', $series->collection()->first());
    }
}
