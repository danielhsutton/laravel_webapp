<?php

namespace App\Policies;

use App\User;
use App\Document;
use App\Collection;
use Illuminate\Auth\Access\HandlesAuthorization;

class DocumentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the document using Eloquent relationship
     * found in app/Document.php. Checks if user can perform same verb action on series
     *
     * @param  \App\User  $user
     * @param  \App\Document  $document
     * @return mixed
     */
    public function view(User $user, Document $document)
    {
        //retreives the series from document relationship using foreign key series_id
        $series = $document->series()->first();

        return $user->can('view', $series);
    }

    /**
     * Determine whether the user can update the document using Eloquent relationship
     * found in app/Document.php. Checks if user can perform same verb action on series
     *
     * @param  \App\User  $user
     * @param  \App\Document  $document
     * @return mixed
     */
    public function update(User $user, Document $document)
    {
        //retreives the series from document relationship using foreign key series_id
        $series = $document->series()->first();

        return $user->can('update', $series);
    }

    /**
     * Determine whether the user can delete the document using Eloquent relationship
     * found in app\Document.php. Checks if user can perform same verb action on series
     *
     * @param  \App\User  $user
     * @param  \App\Document  $document
     * @return mixed
     */
    public function delete(User $user, Document $document)
    {
        //retreives the series from DB with given id
        $series = $document->series()->first();

        return $user->can('delete', $series);

    }
}
