<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'collection_id'
    ];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     *  Series belongs to one Collection
     */
    public function collection()
    {
        //belongsTo because series have collection_id, NOT hasOne, as foreign key is loacated in series table
        return $this->belongsTo('App\Collection', 'collection_id');
    }

    /**
     *  Series has many documents
     */
    public function documents()
    {
        //hasMany as document table has a series_id column
        return $this->hasMany('App\Document');
    }

}
