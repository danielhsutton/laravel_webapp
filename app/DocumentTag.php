<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentTag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'document_id', 'tag_id', 'owner_id', 'text_value', 'int_value', 'tstamp_value', 'created_at', 'updated_at'
    ];

    /**
     *  tag belongs to a user
     */
    public function user()
    {
        //belongsTo because collection_permission has a owner_id, NOT hasOne, as foreign key is loacated in collection_tags table
        return $this->belongsTo('App\User', 'owner_id');
    }

    /**
     *  tag belongs to a collection tag
     */
    public function collection_tag()
    {
        return $this->belongsTo('App\CollectionTag', 'tag_id');
    }

    /**
     *  tag belongs to a document
     */
    public function document()
    {

        return $this->belongsTo('App\Document', 'document_id');
    }
}
