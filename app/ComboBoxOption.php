<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComboBoxOption extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'tag_id', 'value'
    ];


    protected $table = 'document_tags_combo_options';


    /**
     *  option belongs to a collection_tag - many options will belong to a tag to be
     * displayed in a tag select box
     */
    public function collection_tag()
    {
        return $this->belongsTo('App\CollectionTag', 'tag_id');

    }


}
