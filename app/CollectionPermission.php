<?php


namespace App;

define("VIEW", "view");
define("CREATE", "create");
define("UPDATE", "update");
define("DELETE", "delete");
define("ANONYMISED", "anonymised");
define("MANAGE", "manage");


use Enums;
use Illuminate\Database\Eloquent\Model;

class CollectionPermission extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'collection_id', 'owner_id', 'permissions'
    ];
    protected $guarded = ['id', 'created_at', 'updated_at'];


    const permissions = [
        CREATE, VIEW, UPDATE, DELETE, ANONYMISED, MANAGE
    ];


    /**
     *  permissions belongs to a user
     */
    public function user()
    {
        //belongsTo because collection_permission has a owner_id, NOT hasOne, as foreign key is loacated in collection_permission table
        return $this->belongsTo('App\User', 'owner_id');
    }

    /**
     *  permissions belongs to one Collection
     */
    public function collection()
    {
        //belongsTo because collection_permission has a collection_id, NOT hasOne, as foreign key is loacated in collection_permission        return $this->belongsTo('App\Collection', 'collection_id');
        return $this->belongsTo('App\Collection', 'collection_id');
    }

}
