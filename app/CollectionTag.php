<?php

namespace App;

define("STRING", "string");
define("INTEGER", "integer");
define("TIMESTAMP", "timestamp");


use Illuminate\Database\Eloquent\Model;


class CollectionTag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'collection_id', 'owner_id', 'name', 'type', 'category',
    ];

    const types = [
        STRING, INTEGER, TIMESTAMP,
    ];

    /** Returns true if this is a string tag. */
    public function isString() {
        return $this->type == STRING;
    }

    /** Returns true if this is a combo string tag. */
    public function isCombo() {
        $options = $this->comboOptions();
        return $this->isString() && $options->count() > 0;
    }

    /** Returns true if this is an integer tag. */
    public function isInteger() {
        return $this->type == INTEGER;
    }

    /** Returns true if this is a timestamp tag. */
    public function isTimestamp() {
        return $this->type == TIMESTAMP;
    }

    /** Returns true if this tag is a custom tag (has an owner and belongs to a collection)*/
    public function isCustom() {
        return isset($this->collection_id) && isset($this->owner_id);
    }


    /**
     *  tag belongs to a user
     */
    public function user()
    {        //belongsTo because collection_permission has a owner_id, NOT hasOne, as foreign key is loacated in collection_tags table

        return $this->belongsTo('App\User', 'owner_id');
    }

    /**
     *  tag belongs to a collection
     */
    public function collection()
    {
        //belongsTo because collection_permission has a collection_id, NOT hasOne, as foreign key is loacated in collection_tags        return $this->belongsTo('App\Collection', 'collection_id');
        return $this->belongsTo('App\Collection', 'collection_id');
    }

    /**
     *  CollectionTag can have Combo options if it is a select box
     */
    public function comboOptions()
    {
        return $this->hasMany('App\ComboBoxOption', 'tag_id', 'id');
    }




    /**
     *  returns a list of tags avalible to a user on a given collection
     */
    public static function getUserTags($user_id, $collection_id)
    {
            $user = User::find($user_id);
            $collection = Collection::find($collection_id);


            return CollectionTag::where('collection_id', $collection->id)->where('owner_id', $user->id)
                                    ->orWhere('collection_id', null)->orWhere('owner_id', null)->get();

    }

    /**
     *  returns a list of tags avalible to a user on a given collection
     */
    public static function getDefaultTags()
    {
        return CollectionTag::where('collection_id', null)->where('owner_id', null)->get();
    }


    /**
     *  returns a list of tags the user has defined on a given collection
     */
    public static function getUserDefinedTags($user_id, $collection_id)
    {
        $user = User::find($user_id);
        $collection = Collection::find($collection_id);


        return CollectionTag::where('collection_id', $collection->id)->where('owner_id', $user->id)->get();


    }

    /**
     * Return an array of all the tag types, stored as enum values in the collection_tags table
     */
    public static function getTypes() {
        return CollectionTag::types;
    }

    public static function getAllUserTags($user_id){

        return CollectionTag::where('owner_id', $user_id)->get();

    }
}
