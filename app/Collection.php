<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'owner_id',
    ];

    protected $guarded = ['id', 'created_at', 'updated_at'];


    /**
     *  Collection has an owner
     */
    public function owner()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }

    /**
     *  Collection has many series
     */
    public function series()
    {
        //hasMany as series table has a collection_id column
        return $this->hasMany('App\Series', 'collection_id', 'id');
    }

    /**
     *  Collection has permissions
     */
    public function permissions()
    {
        //hasMany as collection_permissions table has a collection_id column
        return $this->hasMany('App\CollectionPermission');
    }

    /**
     *  Collection has many series, which then have many documents
     */
    public function documents()
    {
        //hasMany as series table has a collection_id column
        return $this->hasManyThrough('App\Series', 'App\Document',
                                    'series_id', 'collection_id',
                                    'id', 'id');
    }

    /**
     * returns an array of collections that the user has access to either by owning them
     * or they heve been given access to them
     */
    public static function getCollections($user_id){

        $collections = Collection::where('owner_id', $user_id)->get();

        $col_permissions = CollectionPermission::where('owner_id', $user_id)->where('permissions', 'view')->get();

        if(count($col_permissions)){
            foreach ($col_permissions as $col_permission){
                $collection = Collection::findOrFail($col_permission->collection_id);

                $collections[] = $collection;
            }
        }

        return $collections;
    }

}
