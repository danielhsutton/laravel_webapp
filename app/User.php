<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * A User can have many Collections
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function collections()
    {
        return $this->hasMany('App\Collection', 'owner_id');
    }

    /**
     *  User has many jobs
     */
    public function jobs()
    {
        //hasMany as series table has a collection_id column
        return $this->hasMany('App\Job');
    }

    /**
     * A User has many permissions over collections
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function collectionPermissions()
    {
        //hasMany as collection_permission table has a permission enum column
        return $this->hasMany('App\CollectionPermission');
    }
}
