<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentTagComboOption extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'document_tags_combo_options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tag_id', 'value', 'created_at', 'updated_at'
    ];

}
