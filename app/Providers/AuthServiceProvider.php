<?php

namespace App\Providers;

use App\AnalysisConfig;
use App\AnalysisSet;
use App\Collection;
use App\Series;
use App\Document;
use App\CollectionTag;
use App\Policies\AnalysisSetPolicy;
use App\Policies\AnalysisJobPolicy;
use App\Policies\CollectionTagPolicy;
use App\Policies\CollectionPolicy;
use App\Policies\SeriesPolicy;
use App\Policies\DocumentPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Collection::class => CollectionPolicy::class,
        Series::class => SeriesPolicy::class,
        Document::class => DocumentPolicy::class,
        CollectionTag::class => CollectionTagPolicy::class,
        AnalysisSet::class => AnalysisSetPolicy::class,
        AnalysisConfig::class => AnalysisJobPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
