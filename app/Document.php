<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'anon_title', 'description', 'series_id' ,'raw_doc','URL_page',
        'URL_access_date','doc_type_id'
    ];

    /**
     * @var array These attributes are hidden
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     *  Document belongs to a series
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *          Series object that the Document belongs to using the series_id attribute
     */
    public function series()
    {
        //belongsTo because document have series_id, NOT hasOne, as foreign key is loacated in document table
        return $this->belongsTo('App\Series', 'series_id');
    }

    /**
     *  Document belongs to a series, which belongs to a collection. This returns the collection
     * @return Collection
     */
    public function getCollection()
    {
        return $this->series()->first()->collection()->first();
    }

    /**
     * Returns the value of a collection tag based on its ID determining if it
     * is a string, timestamp, integer or combo
     *
     * @param $col_tag_id
     * @return string
     */
    public function getValueFor($col_tag_id){

        $collectionTag = CollectionTag::findOrFail($col_tag_id);
        $doc_tag = DocumentTag::where('tag_id', $collectionTag->id)->where('document_id', $this->id)->first();

        //when a document is first created, no document_tags are created therefore null is returned.
        //Once document tag value are saved, document tags are created and then values are returned
        if($doc_tag) {

            if($collectionTag->isString()){

                return $doc_tag->text_value;

            }
            elseif($collectionTag->isTimestamp()){

                $datetime = explode( " " , $doc_tag->tstamp_value);
                $formatted_datetime = $datetime[0]."T".$datetime[1];

                return $formatted_datetime;

            }
            elseif($collectionTag->isInteger()) {

                return $doc_tag->int_value;

            }
        }else{
            return null;
        }
    }

    /**
     *  This function takes an array with structure {collection_tag_id => value) and confirms
     * if the document tag values match with the values in the array.
     *
     * If the corresponding collection tag exits, and if there are document tags existing
     * with the collection tag and these document tags have values equal to value is array.
     */
    public function matchesTags($tagFilter){
        foreach ($tagFilter as $key => $value){
            $value_id = preg_split("/(?<=[a-z])(?=\d)/", $key);
            $tag_id = $value_id[1];

            $tag = CollectionTag::findOrFail($tag_id);
            //find collection tag assigned to this document
            $doc_tags = DocumentTag::where('tag_id', $tag->id)->where('document_id', $this->id)->get();

            if (count($doc_tags) == 0) {
                return false;
            }

            foreach ($doc_tags as $doc_tag){
                if ($tag->isString() && $doc_tag->text_value == $value ){
                    // match
                }
                elseif ($tag->isTimestamp() && $doc_tag->tstamp_value == $value){
                    // match
                }
                elseif ($tag->isInteger() && $doc_tag->int_value == $value) {
                    // match
                }
                else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     *  This function checks if document tag text_value is a combo option in the document_tags_combo_options table, if the text_value is also a value
     *  in the combo options table for the same collection tag, return true, otherwise false as it is a not default value
     */
    public function checkIfComboOption($col_tag_id){

        $collectionTag = CollectionTag::findOrFail($col_tag_id);
        if (!$collectionTag->isCombo()) {
            return false;
        }

        $doc_tag = DocumentTag::where('tag_id', $collectionTag->id)->where('document_id', $this->id)->first();
        if (!$doc_tag) {
            return false;
        }

        $combo_options = ComboBoxOption::where('tag_id', $collectionTag->id)->get();
        $combo_option_norm_values = [];
        foreach ($combo_options as $combo_option){
            $combo_option_norm_values[] = $this->normalizeComboValue($combo_option->value);
        }
        $norm_text_value = $this->normalizeComboValue($doc_tag->text_value);

        //if document tag value not in array of combo options, then it is an other value
        return !in_array($norm_text_value, $combo_option_norm_values);
    }

    private function normalizeComboValue($value) {
        $value = str_replace(" ", "", $value);
        $value = strtolower($value);
        return $value;
    }

    /**
     * Returns the true if there is a document tag assigned to the document given a collection tag, otherwise false.
     *
     * @param $col_tag_id
     * @return boolean
     */
    public function checkIfDocTagAssigned($col_tag_id){

        $collectionTag = CollectionTag::findOrFail($col_tag_id);
        $doc_tag = DocumentTag::where('tag_id', $collectionTag->id)->where('document_id', $this->id)->first();
        if($doc_tag){
            return true;
        }

        return false;
    }
}
