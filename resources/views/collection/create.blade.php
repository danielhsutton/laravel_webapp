@extends('layouts.app')
@section('extracss')
    <link rel="stylesheet" href="/js/pdfjs/web/buttonFormat.css">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="col-sm-offset-9 col-sm-3">
                        {{--back button--}}
                        {{ Form::open(array('route' => 'collections.index')) }}
                        {{ Form::hidden('_method', 'GET') }}
                        {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    </div>

                    <div class="col-sm-12 panel-heading">Create a new Collection</div>
                    <div class="col-sm-12 panel-body">

                        {{ Form::open(array('route' => 'collections.store')) }}
                        {{ csrf_field() }}

                        {{ Form::label('name', 'Collection Title') }}
                        {{ Form::text('name', "", ['placeholder' => 'Enter the title']) }}

                        @if ($errors->has('name'))
                            @if ($error = $errors->first('name'))
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endif
                        @endif

                        <div class="col-sm-12">
                            {{ Form::label('description', 'Collection Description') }}
                        </div>
                        <div class="col-sm-12">
                            {{ Form::textarea('description', "", ['placeholder' => 'Enter the description']) }}
                        </div>
                        @if ($errors->has('description'))
                            @if ($error = $errors->first('description'))
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endif
                        @endif
                        <div class="col-sm-12">
                            {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
                            {{ Form::close() }}
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
