@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-8 col-sm-2">
                {{--back button--}}
                {{ Form::open(array('route' => array('collections.show', $collection->id))) }}
                {{ Form::hidden('_method', 'GET') }}
                {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
                {{ Form::close() }}
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading col-sm-10">Edit Collection</div>

                    <div class="panel-body col-sm-12">


                        {{ Form::open(array('route' => array('collections.update', $collection->id))) }}
                        {{ csrf_field() }}
                        {{ method_field("PUT") }}

                        {{ Form::label('name', 'Collection title') }}
                        {{ Form::text('name', $collection->name) }}
                            <br />
                        {{ Form::label('description', 'Collection description') }}
                        {{ Form::textarea('description', $collection->description) }}

                        <div class="col-sm-offset-6">
                            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
