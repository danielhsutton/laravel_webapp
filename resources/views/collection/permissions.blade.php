@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading col-sm-10">Manage Permissions</div>
                    <div class="col-sm-2">
                        {{ Form::open(array('route' => array('collections.show', $collection->id))) }}
                        {{ Form::hidden('_method', 'GET') }}
                        {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    </div>
                    <div class="panel-body">

                        {{ Form::open(array('route' => array('update_permissions', $collection->id))) }}
                        {{ Form::hidden('_method', 'POST') }}

                        <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>

                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>E-mail</th>
                                <th>Create series</th>
                                <th>Full view</th>
                                <th>Anonymised view</th>
                                <th>Update collection</th>
                                <th>Delete collection</th>
                                <th>Allow Management Of Permissions</th>

                            </tr>
                            </thead>

                            <tbody>

                            @foreach($users as $user)
                                <tr>
                                    <td>
                                        {{ $user->firstname }}
                                    </td>
                                    <td>
                                        {{ $user->lastname }}
                                    </td>
                                    <td>
                                        {{ $user->email }}
                                    </td>
                                    <td>
                                        <input name="create[]" type="checkbox" value='{{$user->id}}'
                                           @foreach($permissions as $permission)
                                                @if ($permission->owner_id == $user->id
                                                    && $permission->permissions == 'create')
                                                        checked='true'
                                                @endif
                                            @endforeach
                                        >
                                    </td>
                                    <td>
                                        <input name="view[]" type="checkbox" value='{{$user->id}}'
                                           @foreach($permissions as $permission)
                                               @if ($permission->owner_id == $user->id
                                                   && $permission->permissions == 'view')
                                                        checked='true'
                                                @endif
                                            @endforeach
                                        >
                                    </td>
                                    <td>
                                        <input name="anonymised[]" type="checkbox" value='{{$user->id}}'
                                           @foreach($permissions as $permission)
                                               @if ($permission->owner_id == $user->id
                                                   && $permission->permissions == 'anonymised')
                                                        checked='true'
                                                @endif
                                            @endforeach
                                        >
                                    </td>
                                    <td>
                                        <input name="update[]" type="checkbox" value='{{$user->id}}'
                                           @foreach($permissions as $permission)
                                               @if ($permission->owner_id == $user->id
                                                    && $permission->permissions == 'update')
                                                        checked='true'
                                                @endif
                                            @endforeach
                                        >
                                    </td>
                                    <td>
                                        <input name="delete[]" type="checkbox" value='{{$user->id}}'
                                           @foreach($permissions as $permission)
                                               @if ($permission->owner_id == $user->id
                                                    && $permission->permissions == 'delete')
                                                        checked='true'
                                                @endif
                                            @endforeach
                                        >
                                    </td>
                                    <td>
                                        <input name="manage[]" type="checkbox" value='{{$user->id}}'
                                               @foreach($permissions as $permission)
                                               @if ($permission->owner_id == $user->id
                                                    && $permission->permissions == 'manage')
                                               checked='true'
                                                @endif
                                                @endforeach
                                        >
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div class="col-sm-2 col-sm-offset-10">

                            {{ Form::submit('Apply', array('class' => 'btn btn-primary')) }}
                            {{ Form::close() }}

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
