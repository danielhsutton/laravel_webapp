@extends('layouts.app')

@section('content')
    <br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="panel panel-default col-sm-12">
                <div class="panel-body">
                    <b>Collections</b>
                    @can('create', App\Collection::class)
                    <!-- The Current User Can Create New Collection -->
                        {{--create collections button--}}
                        <a href="{{  route('collections.create') }}"><button role="button" class="btn btn-primary">Create a Collection</button></a>
                    @endcan
                    <table id="collections" class="table table-striped table-bordered table-hover">
                        <tbody>
                        @foreach($collections as $collection)
                            @can('view', $collection)
                                <tr>
                                    <td>
                                        <a href="{{ route('collections.show', $collection->id) }}">{{$collection->name}}</a>
                                    </td>
                                </tr>
                            @endcan
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
