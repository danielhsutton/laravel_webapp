@extends('layouts.app')
@section('extracss')
    <link rel="stylesheet" href="/js/pdfjs/web/buttonFormat.css">
@endsection
@section('content')
    <section class="section-60 section-sm-100">
        <div class="shell text-center text-md-center">
            <div class="range range-md-middle range-md-center">
                <div class="col-sm-offset-4 col-sm-2">
                @can('createSeries', $collection)
                    <!-- The Current User Can Create New Series -->
                        {{--create button--}}
                        {{ Form::open(array('route' => array('series-create', $collection->id ))) }}
                        {{ Form::hidden('_method', 'GET') }}
                        {{ Form::submit('Create Series', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    @endcan
                </div>
                <div class="col-sm-2">
                    @can ('manage', $collection)
                        {{--manage permissions button--}}
                        {{ Form::open(array('route' => array('collection_permission', $collection->id ))) }}
                        {{ Form::hidden('_method', 'GET') }}
                        {{ Form::submit('Manage Permissions', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    @endcan
                </div>

            </div>
        </div>
    </section>

    <div class="row">
        <div class="col-sm-8 col-sm-push-2">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-heading">{{$collection->name}}</h3>
                </div>

                <div class="panel-body content">
                    <p><strong>Description:</strong></p>
                    {!! $collection->description !!}
                </div>

                {{--table for series--}}
                <table class="table table-bordered table-hover table-striped">
                    <tbody>



                    @foreach($series as $series)
                        @can('view', $series)
                            <tr>
                                <td>
                                    <a href="{{ route('series.show', $series->id) }}"/>{{ $series->name }}
                                </td>
                            </tr>
                        @endcan
                    @endforeach
                    </tbody>
                </table>

            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-3">
                @can('update', $collection)
                    <!-- The Current User Can edit The collection -->
                        {{--edit button--}}
                        {{ Form::open(array('route' => array('collections.edit', $collection))) }}
                        {{ Form::hidden('_method', 'GET') }}
                        {{ Form::submit('Edit collection', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    @endcan
                </div>
                <div class="col-sm-3">
                    @can('delete', $collection)
                        {{--delete button--}}
                        {{ Form::open(array('route' => array('collections.destroy', $collection))) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('Delete collection', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    @endcan
                </div>
                <div class="col-sm-3">
                    {{--back button--}}
                    {{ Form::open(array('route' => 'collections.index')) }}
                    {{ Form::hidden('_method', 'GET') }}
                    {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
                    {{ Form::close() }}
                </div>
            </div>


        </div>
    </div>

@endsection
