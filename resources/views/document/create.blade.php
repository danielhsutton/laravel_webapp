@extends('layouts.app')
@section('extracss')
    <link rel="stylesheet" href="/js/pdfjs/web/buttonFormat.css">
@endsection
@section('content')
    <div class="container">
        <div class="col-sm-4 col-sm-offset-8">
            {{--back button--}}
            {{ Form::open(array('route' => array('series.show', $series->id))) }}
            {{ Form::hidden('_method', 'GET') }}
            {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create A New Document</div>

                    <div class="panel-body">
                        <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{ route('documents.store') }}">
                            {{ csrf_field() }}
                            {{ Form::hidden('series_id', $series->id) }}

                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title</label>
                                <div class="col-md-6">
                                    <input type="text" name="title" id="title" class="form-control" placeholder="Enter the title"  autofocus>
                                </div>

                            </div>
                            @if ($errors->has('title'))
                                @if ($error = $errors->first('title'))
                                    <div class="alert alert-danger">
                                        {{ $error }}
                                    </div>
                                @endif
                            @endif

                            <div class="form-group">
                                <label for="description" class="col-md-4 control-label">Description</label>
                                <div class="col-md-6">
                              <textarea
                                      id="description"
                                      name="description" class="form-control"
                                      placeholder="Enter the description" ></textarea>
                                </div>

                            </div>
                            @if ($errors->has('description'))
                                @if ($error = $errors->first('description'))
                                    <div class="alert alert-danger">
                                        {{ $error }}
                                    </div>
                                @endif
                            @endif
                            <div class="form-group">
                                <label for="document" class="col-md-4 control-label">Add a document file</label>
                                <div class="col-md-6">
                                    {{Form::file('document', ['class' => 'form-control', 'accept' => 'application/pdf'])}}
                                </div>

                            </div>
                            @if ($errors->has('document'))
                                @if ($error = $errors->first('document'))
                                    <div class="alert alert-danger">
                                        {{ $error }}
                                    </div>
                                @endif
                            @endif

                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Anonymised Title</label>
                                <div class="col-md-6">
                                    <input type="text" name="anontitle" id="anontitle" class="form-control" placeholder="Enter the anonymised title" autofocus>
                                </div>

                            </div>
                            @if ($errors->has('anontitle'))
                                @if ($error = $errors->first('anontitle'))
                                    <div class="alert alert-danger">
                                        {{ $error }}
                                    </div>
                                @endif
                            @endif

                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">URL page</label>
                                <div class="col-md-6">
                                    <input type="text" name="urlpage" id="urlpage" class="form-control" autofocus>
                                </div>

                            </div>
                            @if ($errors->has('urlpage'))
                                @if ($error = $errors->first('urlpage'))
                                    <div class="alert alert-danger">
                                        {{ $error }}
                                    </div>
                                @endif
                            @endif
                            <div class="form-group">
                                <label for="date" class="col-md-4 control-label">URL accessed date</label>
                                <div class="col-md-6">
                                    <input type="date" name="date" id="date" class="form-control"
                                           >

                                </div>
                            </div>
                            @if ($errors->has('date'))
                                @if ($error = $errors->first('date'))
                                    <div class="alert alert-danger">
                                        {{ $error }}
                                    </div>
                                @endif
                            @endif

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">

                                        <input type="submit" class="btn btn-primary" value="Create">

                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
