@extends('layouts.app')
<!--extend the main template so you can then add it-->
<!--Extend the extracss and extrajs sections to add the css style sheets and javascript specific to this page-->
@section('extracss')
    <link rel="stylesheet" href="/js/pdfjs/web/viewer.css">
    <link rel="resource" type="application/l10n" href="/js/pdfjs/web/locale/locale.properties">
@endsection
@section('extrajs')
    <script src="/js/pdfjs/build/pdf.worker.js"></script>
    <script src="/js/pdfjs/build/pdf.js"></script>
    <script src="/js/pdfjs/web/viewer.js"></script>

    <script src="/js/form.js"></script>
@endsection
<!--Extend the content section to fill in the content of the page-->
@section('content')
    <section class="section-60 section-sm-100">
        <div class="shell text-center text-md-center">
            <div class="range range-md-middle range-md-center">

                {{--Create custom tag button--}}
                @can('update', $document)
                    <a href="{{  route('create-tag', ['document_id'=>$document->id]) }}"><button role="button" class="btn btn-primary">Create Custom Tag</button></a>
                @endcan

                {{--back button--}}
                <a href="{{  route('documents.show', ['document_id'=>$document->id]) }}"><button role="button" class="btn btn-primary">Back</button></a>

                {{--save button--}}
                <button type="submit" role="button" form="doc_tags_form" class="btn btn-primary">Save</button>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="panel panel-default">

            <div class="panel-heading">
                <h3 class="panel-heading">{{$document->title}}</h3>
            </div>

            <div class="panel-body content">

                <div class="col-sm-6">
                    @if($document->raw_doc != null)
                        <script type="text/javascript" >
                            $(function() { PDFViewerApplication.open('/documents/{{$document->id}}/pdf'); });
                        </script>
                        <div class="panel-body content">
                            <p><strong>Document</strong></p>
                            <div style="width:500px; height: 700px; border: solid;" >
                                @include('document.pdfjs')
                                <div id="printContainer"></div>
                            </div>

                        </div>
                    @endif
                </div>

                <div class="col-sm-6">

                    <form class="form-horizontal" method="POST" action="{{ route('store-tags', [$document->id, Auth::user()->id]) }}" id="doc_tags_form">
                    {{ csrf_field() }}

                        {{--this displays the tag headers and tags with no checkboxes and with values--}}
                        @include('components.tag_input', ['checkboxes' => false])

                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
