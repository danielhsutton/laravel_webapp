@extends('layouts.app')
<!--extend the main template so you can then add it-->
<!--Extend the extracss and extrajs sections to add the css style sheets and javascript specific to this page-->
@section('extracss')
    <link rel="stylesheet" href="/js/pdfjs/web/viewer.css">
    <link rel="resource" type="application/l10n" href="/js/pdfjs/web/locale/locale.properties">
    <link rel="stylesheet" href="/js/pdfjs/web/buttonFormat.css">
@endsection
@section('extrajs')
    <script src="/js/pdfjs/build/pdf.worker.js"></script>
    <script src="/js/pdfjs/build/pdf.js"></script>
    <script src="/js/pdfjs/web/viewer.js"></script>
@endsection
<!--Extend the content section to fill in the content of the page-->
@section('content')
    <div class="col-sm-offset-9">
        {{--back button--}}
        {{ Form::open(array('route' => array('documents.show', $document->id))) }}
        {{ Form::hidden('_method', 'GET') }}
        {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>

    <div class="container">
        <div class="panel panel-default">

            <div class="panel-heading col-sm-8">
                <h3 class="panel-heading">{{$document->title}}</h3>
            </div>
                <div class="panel-body content">

                    <div class="col-xs-12 col-sm-6">
                        @if($document->raw_doc != null)
                            <script type="text/javascript" >
                                $(function() { PDFViewerApplication.open('/documents/{{$document->id}}/pdf'); });
                            </script>
                            <div class="panel-body content">
                                <p><strong>Document</strong></p>
                                <div style="width:500px; height: 700px; border: solid;" >
                                    @include('document.pdfjs')
                                    <div id="printContainer"></div>
                                </div>

                            </div>
                        @endif
                    </div>

                    <div class="col-xs-12 col-sm-6">

                        {{ Form::open(array('route' => array('store-digitised', $document->id))) }}
                        {{ csrf_field() }}

                        {{ Form::label('text', 'Digitised Text') }}
                        {{ Form::textarea('text', $document->digitised_text, ['style'=>'width:100%; border:inset;'], ['placeholder' => 'Enter the digitised text']) }}

                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}

                    </div>
                </div>
            </div>
    </div>
@endsection
