@extends('layouts.app')
<!--extend the main template so you can then add it-->
<!--Extend the extracss and extrajs sections to add the css style sheets and javascript specific to this page-->
@section('extracss')
    <link rel="stylesheet" href="/js/pdfjs/web/viewer.css">
    <link rel="stylesheet" href="/js/pdfjs/web/buttonFormat.css">
    <link rel="resource" type="application/l10n" href="/js/pdfjs/web/locale/locale.properties">
@endsection
@section('extrajs')
    <script src="/js/pdfjs/build/pdf.worker.js"></script>
    <script src="/js/pdfjs/build/pdf.js"></script>
    <script src="/js/pdfjs/web/viewer.js"></script>
@endsection
<!--Extend the content sectionto fill in the content of the page-->
@section('content')

    <section class="section-60 section-sm-100">
        <div class="shell text-center text-md-center">
            <div class="range range-md-middle range-md-center">

                @can('update', $document)
                    <a href="{{  route('create-digitised', ['document_id' => $document->id]) }}"><button role="button" class="btn btn-primary">Digitise Text</button></a>
                @endcan

                @can('update', $document)
                    <a href="{{  route('add-tags', ['document_id'=>$document->id, 'user_id'=>Auth::user()->id]) }}"><button role="button" class="btn btn-primary">Manage Tags</button></a>
                @endcan

                @can('update', $document)
                    <a href="{{  route('view-tags', ['document_id'=>$document->id, 'user_id'=>Auth::user()->id]) }}"><button role="button" class="btn btn-primary">Manage Custom Tags</button></a>
                @endcan

            </div>
        </div>
    </section>

    <!--Buffer between rows to stop overlapping-->
    <div style="height: 25px;"></div>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-2">
            @can('update', $document)
            <!-- The Current User Can edit The document -->
                {{--edit button--}}
                {{ Form::open(array('route' => array('documents.edit', $document->id))) }}
                {{ Form::hidden('_method', 'GET') }}
                {{ Form::submit('Edit Document', array('class' => 'btn btn-primary')) }}
                {{ Form::close() }}
            @endcan
        </div>
        <div class="col-sm-2">
            @can('delete', $document)
                {{--delete button--}}
                {{ Form::open(array('route' => array('documents.destroy', $document->id))) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete Document', array('class' => 'btn btn-primary')) }}
                {{ Form::close() }}
            @endcan
        </div>
        <div class="col-sm-2">
                {{--back button--}}
                {{ Form::open(array('route' => array('series.show', $document->series_id))) }}
                {{ Form::hidden('_method', 'GET') }}
                {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
                {{ Form::close() }}
        </div>
    </div>

    <!--Buffer between rows to stop overlapping-->
    <div style="height: 25px;"></div>

    <div class="container">
        <div class="panel panel-default">

            <div class="panel-heading">
                <h3 class="panel-heading">{{$document->title}}</h3>

                <p><strong>Description:</strong></p>
                {!! $document->description !!}
            </div>

            <div class="panel-body content">

                <div>
                    @if($document->raw_doc != null)
                        <script type="text/javascript" >
                            $(function() { PDFViewerApplication.open('/documents/{{$document->id}}/pdf'); });
                        </script>
                        <div class="panel-body content">
                            <p><strong>Document</strong></p>
                            <div style="width:100%; height: 900px; border: solid;" >
                                @include('document.pdfjs')
                                <div id="printContainer"></div>
                            </div>

                        </div>
                    @endif
                </div>

                @if($document->digitised_text != null)
                    <div class="panel-body content">
                        <p><strong>Digitised Document</strong></p>
                        <div>{{$document->digitised_text}}</div>
                    </div>
                @endif

            </div>
        </div>
    </div>

@endsection
