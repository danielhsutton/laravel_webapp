@extends('layouts.app')
@section('extracss')
    <link rel="stylesheet" href="/js/pdfjs/web/buttonFormat.css">
@endsection
@section('content')
    <section class="section-60 section-sm-100">
        <div class="shell text-center text-md-center">
            <div class="range range-md-middle range-md-center">

            @can('createDocument', $series)
                <!-- The Current User Can Add A New Document -->
                    {{--create button--}}
                    {{ Form::open(array('route' => array('document-create', $series->id ))) }}
                    {{ Form::hidden('_method', 'GET') }}
                    {{ Form::submit('Create Document', array('class' => 'btn btn-primary')) }}
                    {{ Form::close() }}
            @endcan

            </div>
        </div>
    </section>

     <div class="row">
        <div class="col-sm-8 col-sm-push-2">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-heading">{{$series->name}}</h3>
                </div>

                <div class="panel-body content">
                    <p><strong>Description:</strong></p>
                    {!! $series->description !!}
                </div>

                {{--table for series--}}
                <table class="table table-bordered table-hover table-striped">
                    <tbody>
                    @foreach($documents as $document)
                        @can('view', $document)
                            <tr>
                                <td>
                                    <a href="{{ route('documents.show', $document->id) }}"/>{{ $document->title }}
                                </td>
                            </tr>
                        @endcan
                    @endforeach
                    </tbody>
                </table>

            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-3">
                    @can('update', $series)
                    <!-- The Current User Can edit The collection -->
                        {{--edit button--}}
                        {{ Form::open(array('route' => array('series.edit', $series))) }}
                        {{ Form::hidden('_method', 'GET') }}
                        {{ Form::submit('Edit Series', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    @endcan
                </div>
                <div class="col-sm-3">
                    @can('delete', $series)
                        {{--delete button--}}
                        {{ Form::open(array('route' => array('series.destroy', $series))) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('Delete series', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    @endcan
                </div>
                <div class="col-sm-3">
                    {{--back button--}}
                    {{ Form::open(array('route' => array('collections.show', $series->collection_id))) }}
                    {{ Form::hidden('_method', 'GET') }}
                    {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
                    {{ Form::close() }}
                </div>

            </div>


        </div>
    </div>

@endsection
