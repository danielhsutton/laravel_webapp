@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">edit series</div>

                    <div class="panel-body">

                        {{ Form::open(array('route' => array('series.update', $series->id))) }}
                        {{ csrf_field() }}
                        {{ method_field("PUT") }}

                        {{ Form::label('name', 'Series title') }}
                        {{ Form::text('name', $series->name) }}

                        @if ($errors->has('name'))
                            @if ($error = $errors->first('name'))
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endif
                        @endif

                        {{ Form::label('description', 'Series description') }}
                        {{ Form::text('description', $series->description) }}

                        @if ($errors->has('description'))
                            @if ($error = $errors->first('description'))
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endif
                        @endif

                        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}

                        <div class="col-md-8 col-md-offset-4">
                            {{--back button--}}
                            {{ Form::open(array('route' => array('series.show', $series->id))) }}
                            {{ Form::hidden('_method', 'GET') }}
                            {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
