@extends('layouts.app')
@section('extracss')
    <link rel="stylesheet" href="/js/pdfjs/web/buttonFormat.css">
@endsection
@section('content')
    <div class="container">
        <div class="col-sm-4 col-sm-offset-8">
            {{--back button--}}
            {{ Form::open(array('route' => array('collections.show', $collection->id))) }}
            {{ Form::hidden('_method', 'GET') }}
            {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
        <div class="row">
            <div class="col-sm-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create A New Series</div>

                    <div class="panel-body">

                        {{ Form::open(array('route' => 'series.store')) }}
                        {{ csrf_field() }}
                        {{ Form::hidden('collection_id', $collection->id) }}

                        <div class="col-sm-12">
                            {{ Form::label('name', 'Series title') }}
                            {{ Form::text('name', "", ['style'=>'border-bottom-width:1px;'], ['placeholder' => 'Enter the title']) }}
                        </div>
                        @foreach ($errors->get('name') as $error)
                            <div class="alert alert-danger">{{ $error }}</div>
                        @endforeach

                        <div class="col-sm-12">
                            {{ Form::label('description', 'Series description') }}
                            {{ Form::text('description', "", ['style'=>'width:100%; height:100px; border-bottom-width:1px;']) }}
                        </div>
                        @foreach ($errors->get('description') as $error)
                            <div class="alert alert-danger">{{ $error }}</div>
                        @endforeach

                        {{ Form::submit('Create', array('class' => 'btn btn-primary'), ["style"=>"padding:20px;"]) }}
                        {{ Form::close() }}


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
