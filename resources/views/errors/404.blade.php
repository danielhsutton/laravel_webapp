
@extends('layouts.app')
@section('content')

    @guest
        <script>window.location = "/login";</script>
    @else
        <div class="row">
            <div class="col-sm-8 col-sm-push-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-heading">There was an unexpected error</h3>
                    </div>


                    <div class="panel-body">

                        <div class="alert alert-danger">
                            @if(isset($message))
                                {{ $message }}
                            @endif
                        </div>

                        <button type="submit" onclick="location.href = '/collections';" class="btn btn-primary">
                            Back
                        </button>

                    </div>

                </div>

            </div>
        </div>
    @endguest
@endsection
