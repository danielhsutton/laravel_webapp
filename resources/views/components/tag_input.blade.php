{{--
This is a component that will be used when ever the user is required to see/input values
for tags. Users creating Analysis sets will be required to check which tags they want to
apply to the analysis and also the values of the analysis. Users will also want to insert
values for the tags when creating Document Tags.

@param checkbox
    For an analysis set, checkboxes are required for a user to input which tags will be used but not when adding
    document tags for a documents. If parameter is true, checkboxes will appear. If false, the values of set
    document tags will appear.
--}}

<!-- Nav tabs -->
<ul class="nav nav-tabs nav-justified">
    @foreach($categories as $category)
        <li class="nav-item">
            <a @if($categories[0]===$category)class="nav-link active" @else class="nav-link" @endif data-toggle="tab" href="#{{$category->category}}" role="tab">{{$category->category}}</a>
        </li>
    @endforeach
</ul>

<!-- Tab panels -->
<div class="tab-content card">

    @foreach($categories as $category)
        <div @if($categories[0]===$category)class="tab-pane fade in active"
             @else class="tab-pane fade in" @endif  id="{{$category->category}}"
             role="tabpanel">
            <br>
            @foreach($tags as $tag)

                {{--if the use can use the tag, they can use if they own it or it is default, or if they have the correct use permission--}}
                @can('use', $tag)
                    @if($checkboxes)
                        {{--if the tag is custom, hide it--}}
                        @if($tag->isCustom())
                            <span class="customTag"
                                  id="{{$tag->collection_id}}"
                                  style="display: none;">
                        @endif
                    @endif

                    @if($tag->category == $category->category)

                        @if($checkboxes)
                            <div class="col-md-8">
                                <input type="checkbox" name="check_form_data[value{{$tag->id}}]" value='{{$tag->id}}'>
                            </div>
                        @endif
                        <div class="col-md-8">
                            <label>{{$tag->name}}</label>
                        </div>
                        <div class="col-md-8">
                            <input type="hidden" name="tag_ids[]" value="{{$tag->id}}">

                            @if($tag->isCombo())

                                <div class="col-md-8">
                                    <select name="form_data[value{{$tag->id}}]" class="selectBox form-control resizedBox">
                                        @foreach($tag->comboOptions()->get() as $comboOption)
                                            <option @if(!$checkboxes) @if(!$document->checkIfDocTagAssigned($tag->id) && $comboOption->value == '[not set]')selected @elseif($document->getValueFor($tag->id, Auth::user()->id, $document->id) === $comboOption->value)selected @elseif($document->checkIfComboOption($tag->id) && $comboOption->value == 'Other' )selected @endif @endif value="{{$comboOption->value}}" @if($comboOption->value === 'Other')data-show="other{{$tag->id}}"@endif>{{$comboOption->value}}</option>
                                           {{--This is a clear format of the above statement--}}
                                            {{--<option--}}
                                                {{--@if(!$checkboxes)--}}
                                                    {{--@if(!$document->checkIfDocTagAssigned($tag->id) && $comboOption->value == '[not set]')--}}
                                                        {{--selected--}}
                                                    {{--@elseif($document->getValueFor($tag->id, Auth::user()->id, $document->id) === $comboOption->value)--}}
                                                        {{--selected--}}
                                                    {{--@elseif($document->checkIfComboOption($tag->id) && $comboOption->value == 'Other' )--}}
                                                        {{--selected--}}
                                                    {{--@endif--}}
                                                {{--@endif--}}
                                                {{--value="{{$comboOption->value}}"--}}
                                                {{--@if($comboOption->value === 'Other')--}}
                                                    {{--data-show="other{{$tag->id}}"--}}
                                                {{--@endif>{{$comboOption->value}}--}}
                                            {{--</option>--}}
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-8" id="other{{$tag->id}}" style="display: none;">
                                    <label>{{$tag->name}} 'Other' Specify</label><br/>
                                    <input class="resizedBox" type="text" name="form_data[value{{$tag->id}}other]" @if(!$checkboxes) @if($document->checkIfComboOption($tag->id))value="{{$document->getValueFor($tag->id, Auth::user()->id)}}"@endif @endif placeholder="Specify Other">
                                </div>
                            @elseif($tag->isString())
                                <input class="resizedBox" type="text" name="form_data[value{{$tag->id}}]" @if(!$checkboxes)value="{{$document->getValueFor($tag->id, Auth::user()->id)}}" @endif>
                            @elseif($tag->isTimestamp())
                                <input class="resizedBox" type="datetime-local" name="form_data[value{{$tag->id}}]" @if(!$checkboxes)value="{{$document->getValueFor($tag->id, Auth::user()->id)}}" @endif>
                            @elseif($tag->isInteger())
                                <input class="resizedBox" type="number" name="form_data[value{{$tag->id}}]" @if(!$checkboxes)value="{{$document->getValueFor($tag->id, Auth::user()->id)}}" @endif min="0" max="2147483647">
                            @endif
                        </div>
                    @endif

                    @if($tag->isCustom())
                        </span>
                    @endif
                @endcan
            @endforeach
        </div>
    @endforeach
</div>
