@extends('layouts.app')
@section('extrajs')
    <script src="/js/form.js"></script>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Update tag for {{$tag->name}}</div>

                    <div class="panel-body">

                        {{ Form::open(array('route' => array('tag.update', $tag->id )))}}

                        {{ csrf_field() }}
                        {{ method_field("PUT") }}
                        {{ Form::hidden('user_id', Auth::user()->id) }}
                        {{ Form::hidden('document_id', $document->id) }}



                    @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input type="text" name="name" id="name" class="form-control" value="{{$tag->name}}">

                                </div>


                            </div>


                            {{--<div class="form-group">--}}
                                {{--<label for="description" class="col-md-4 control-label">Category</label>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<select id="type" name="category" class="form-control selectBox">--}}
                                        {{--@if ($tag->category == 'Document')--}}
                                            {{--<option selected>Document</option>--}}
                                            {{--<option>Sender</option>--}}
                                            {{--<option>Recipient</option>--}}
                                        {{--@elseif ($tag->category == 'Sender')--}}
                                            {{--<option selected>Sender</option>--}}
                                            {{--<option>Document</option>--}}
                                            {{--<option>Recipient</option>--}}

                                        {{--@elseif ($tag->category == 'Recipient')--}}
                                            {{--<option selected>Recipient</option>--}}
                                            {{--<option>Document</option>--}}
                                            {{--<option>Sender</option>--}}
                                        {{--@endif--}}
                                    {{--</select>--}}

                                {{--</div>--}}

                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label for="description" class="col-md-4 control-label">Type</label>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<select id="type" name="type" class="form-control selectBox">--}}
                                        {{--@if ($tag->isCombo())--}}
                                            {{--<option>string</option>--}}
                                            {{--<option>integer</option>--}}
                                            {{--<option>timestamp</option>--}}
                                            {{--<option selected data-show="1">combo</option>--}}
                                        {{--@elseif ($tag->isInteger())--}}
                                            {{--<option>string</option>--}}
                                            {{--<option selected>integer</option>--}}
                                            {{--<option>timestamp</option>--}}
                                            {{--<option data-show="1">combo</option>--}}
                                        {{--@elseif ($tag->isTimestamp())--}}
                                            {{--<option>string</option>--}}
                                            {{--<option >integer</option>--}}
                                            {{--<option selected>timestamp</option>--}}
                                            {{--<option data-show="1">combo</option>--}}
                                        {{--@elseif ($tag->isString())--}}
                                            {{--<option selected>string</option>--}}
                                            {{--<option >integer</option>--}}
                                            {{--<option >timestamp</option>--}}
                                            {{--<option data-show="1">combo</option>--}}


                                        {{--@endif--}}

                                    {{--</select>--}}



                                {{--</div>--}}

                            {{--</div>--}}


                            <div class="form-group" id="1" style="display: none;">
                                <label class="col-md-4 control-label">Combo Options</label>
                                <div class="col-md-4" id="options">
                                    <button type="button" class="btn btn-primary" id="added" data-show="add">Add</button>
                                    <button type="button" class="btn btn-primary " id="removed">Remove</button>

                                    <input type="text" name="options[]" class="form-control" placeholder="Enter an option">
                                    <input type="text" name="options[]" class="form-control" placeholder="Enter an option">

                                </div>

                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}


                                </div>

                            </div>

                        {{ Form::close() }}


                        <div class="col-md-8 col-md-offset-4">
                            {{--back button--}}
                            {{ Form::open(array('route' => array('view-tags', $document->id, 'user_id'=>Auth::user()->id))) }}
                            {{ Form::hidden('_method', 'GET') }}
                            {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
                            {{ Form::close() }}
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

