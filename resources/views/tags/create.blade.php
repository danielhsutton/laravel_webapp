@extends('layouts.app')
@section('extrajs')
   <script src="/js/form.js"></script>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create a new Tag to use</div>

                    <div class="panel-body">



                        <form class="form-horizontal" method="POST" action="{{ route('tag.store') }}">
                            {{ csrf_field() }}
                            {{ Form::hidden('user_id', Auth::user()->id) }}
                            {{ Form::hidden('document_id', $document->id) }}

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Enter the name">

                                </div>


                            </div>


                            <div class="form-group">
                                <label for="category" class="col-md-4 control-label">Category</label>
                                <div class="col-md-6">
                                    <select id="type" name="category" class="form-control selectBox">
                                        <option>Document</option>
                                        <option>Sender</option>
                                        <option>Recipient</option>
                                    </select>

                                </div>

                            </div>

                            <div class="form-group">
                                <label for="type" class="col-md-4 control-label">Type</label>
                                <div class="col-md-6">
                                    <select id="type" name="type" class="form-control selectBox">
                                        <option selected disabled hidden>Please select one option</option>

                                        @foreach($types as $type)

                                            @if($type=='combo')
                                                <option data-show="1">combo</option>
                                            @else
                                                <option>{{$type}}</option>
                                            @endif
                                        @endforeach

                                    </select>



                                </div>

                            </div>


                            <div class="form-group" id="1" style="display: none;">
                                <label class="col-md-4 control-label">Combo Options</label>
                                <div class="col-md-4" id="options">
                                    <button type="button" class="btn btn-primary" id="added" data-show="add">Add</button>
                                    <button type="button" class="btn btn-primary " id="removed">Remove</button>

                                    <input type="text" name="options[]" class="form-control" placeholder="Enter an option">
                                    <input type="text" name="options[]" class="form-control" placeholder="Enter an option">

                                </div>


                            </div>
                            

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">

                                    <input type="submit" class="btn btn-primary" value="Create">

                                </div>

                            </div>

                        </form>


                        <div class="col-md-8 col-md-offset-4">
                            {{--back button--}}
                            {{ Form::open(array('route' => array('add-tags', $document->id, 'user_id'=>Auth::user()->id))) }}
                            {{ Form::hidden('_method', 'GET') }}
                            {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
                            {{ Form::close() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

