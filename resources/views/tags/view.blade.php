@extends('layouts.app')
<!--extend the main template so you can then add it-->
<!--Extend the extracss and extrajs sections to add the css style sheets and javascript specific to this page-->
@section('extracss')
    <link rel="stylesheet" href="/js/pdfjs/web/viewer.css">
    <link rel="resource" type="application/l10n" href="/js/pdfjs/web/locale/locale.properties">
@endsection
@section('extrajs')
    <script src="/js/pdfjs/build/pdf.worker.js"></script>
    <script src="/js/pdfjs/build/pdf.js"></script>
    <script src="/js/pdfjs/web/viewer.js"></script>

    <script src="/js/tag_form.js"></script>
@endsection
<!--Extend the content section to fill in the content of the page-->
@section('content')
    <section class="section-60 section-sm-100">
        <div class="shell text-center text-md-center">
            <div class="range range-md-middle range-md-center">

                @can('update', $document)
                    <!-- The Current User Can Create New Tag -->
                        {{--create button--}}
                        {{ Form::open(array('route' => array('create-tag', $document->id ))) }}
                        {{ Form::hidden('_method', 'GET') }}
                        {{ Form::submit('Create Tag', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    @endcan

                    {{ Form::open(array('route' => array('documents.show', $document->id))) }}
                    {{ Form::hidden('_method', 'GET') }}
                    {{ Form::submit('Back', array('class' => 'btn btn-primary')) }}
                    {{ Form::close() }}

                </div>

        </div>
    </section>

    <div class="container">

        <div class="panel panel-default">

            <div class="panel-heading">
                <h3 class="panel-heading">{{$document->title}}</h3>
            </div>

            <div class="panel-body content">

                <div class="col-sm-6">
                    @if($document->raw_doc != null)
                        <script type="text/javascript" >
                            $(function() { PDFViewerApplication.open('/documents/{{$document->id}}/pdf'); });
                        </script>
                        <div class="panel-body content">
                            <p><strong>Document</strong></p>
                            <div style="width:500px; height: 700px; border: solid;" >
                                @include('document.pdfjs')
                                <div id="printContainer"></div>
                            </div>

                        </div>
                    @endif
                </div>





                <div class="col-sm-6">


                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h3 class="panel-heading">User Defined Tags for {{$document->title}}</h3>
                        </div>

                        <div class="panel-body content">



                            <ul class="nav nav-tabs nav-justified">

                                @foreach($categories as $category)
                                    <li class="nav-item">
                                        <a @if($categories[0]===$category)class="nav-link active"@else class="nav-link"@endif data-toggle="tab" href="#{{$category->category}}" role="tab">{{$category->category}}</a>
                                    </li>

                                @endforeach

                            </ul>

                            <!-- Tab panels -->
                            <div class="tab-content card">


                                @foreach($categories as $category)
                                    <div @if($categories[0]===$category)class="tab-pane fade in active"@else class="tab-pane fade in"@endif  id="{{$category->category}}" role="tabpanel">
                                        <br>


                                        @foreach($userDefinedCollectionTags as $userDefinedCollectionTag)


                                            @if($userDefinedCollectionTag->category == $category->category)

                                                <div class="col-md-8">
                                                    <label>{{$userDefinedCollectionTag->name}}</label>
                                                </div>

                                                <div class="col-md-8">

                                                    <input type="hidden" name="tag_ids[]" value="{{$userDefinedCollectionTag->id}}">


                                                    @if($userDefinedCollectionTag->isString())



                                                        @if($userDefinedCollectionTag->isCombo())

                                                            <div class="col-md-8">

                                                                <select name="form_data[]" class="selectBox form-control">
                                                                    @foreach($userDefinedCollectionTag->comboOptions()->get() as $comboOption)
                                                                        <option value="{{$comboOption->value}}">{{$comboOption->value}}</option>
                                                                    @endforeach
                                                                </select>

                                                            </div>


                                                            {{ Form::open(array('route' => array('edit-tags', $document->id, Auth::User()->id,  $userDefinedCollectionTag->id))) }}
                                                            {{ Form::hidden('_method', 'GET') }}
                                                            {{ Form::submit('Edit Tag', array('class' => 'btn btn-primary btn-sm'))}}
                                                            {{ Form::close() }}
                                                            {{ Form::open(array('route' => array('tag.destroy', $userDefinedCollectionTag->id))) }}
                                                            {{ Form::hidden('_method', 'DELETE') }}
                                                            {{ Form::hidden('user_id', Auth::user()->id) }}
                                                            {{ Form::hidden('document_id', $document->id) }}
                                                            {{ Form::submit('Delete tag', array('class' => 'btn btn-primary btn-sm')) }}
                                                            {{ Form::close() }}

                                                        @else
                                                            <input type="text" name="form_data[]" value="{{$document->getValueFor($userDefinedCollectionTag->id, Auth::user()->id,$document->id)}}">

                                                            {{ Form::open(array('route' => array('edit-tags', $document->id, Auth::User()->id, $userDefinedCollectionTag->id))) }}
                                                            {{ Form::hidden('_method', 'GET') }}
                                                            {{ Form::submit('Edit Tag', array('class' => 'btn btn-primary btn-sm'))}}
                                                            {{ Form::close() }}

                                                            {{ Form::open(array('route' => array('tag.destroy', $userDefinedCollectionTag->id))) }}
                                                            {{ Form::hidden('_method', 'DELETE') }}
                                                            {{ Form::hidden('user_id', Auth::user()->id) }}
                                                            {{ Form::hidden('document_id', $document->id) }}
                                                            {{ Form::submit('Delete tag', array('class' => 'btn btn-primary btn-sm')) }}
                                                            {{ Form::close() }}

                                                        @endif


                                                    @elseif($userDefinedCollectionTag->isTimestamp())

                                                        <input type="datetime-local" name="form_data[]" value="{{$document->getValueFor($userDefinedCollectionTag->id, Auth::user()->id,$document->id)}}">

                                                        {{ Form::open(array('route' => array('edit-tags', $document->id, Auth::User()->id,  $userDefinedCollectionTag->id))) }}
                                                        {{ Form::hidden('_method', 'GET') }}
                                                        {{ Form::submit('Edit Tag', array('class' => 'btn btn-primary btn-sm'))}}
                                                        {{ Form::close() }}
                                                        {{ Form::open(array('route' => array('tag.destroy', $userDefinedCollectionTag->id))) }}
                                                        {{ Form::hidden('_method', 'DELETE') }}
                                                        {{ Form::hidden('user_id', Auth::user()->id) }}
                                                        {{ Form::hidden('document_id', $document->id) }}
                                                        {{ Form::submit('Delete tag', array('class' => 'btn btn-primary btn-sm')) }}
                                                        {{ Form::close() }}

                                                    @elseif($userDefinedCollectionTag->isInteger())

                                                        <input type="number" name="form_data[]" min="0" class="form-control" value="{{$document->getValueFor($userDefinedCollectionTag->id, Auth::user()->id,$document->id)}}">

                                                        {{ Form::open(array('route' => array('edit-tags', $document->id, Auth::User()->id,  $userDefinedCollectionTag->id))) }}
                                                        {{ Form::hidden('_method', 'GET') }}
                                                        {{ Form::submit('Edit Tag', array('class' => 'btn btn-primary btn-sm'))}}
                                                        {{ Form::close() }}
                                                        {{ Form::open(array('route' => array('tag.destroy', $userDefinedCollectionTag->id))) }}
                                                        {{ Form::hidden('_method', 'DELETE') }}
                                                        {{ Form::hidden('user_id', Auth::user()->id) }}
                                                        {{ Form::hidden('document_id', $document->id) }}
                                                        {{ Form::submit('Delete tag', array('class' => 'btn btn-primary btn-sm')) }}
                                                        {{ Form::close() }}

                                                    @endif

                                                </div>

                                            @endif

                                        @endforeach

                                    </div>

                                @endforeach

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
