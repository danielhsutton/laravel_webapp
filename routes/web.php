<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

Route::resource('collections', 'CollectionController');
Route::get('collections/{collection_id}/create_series', 'SeriesController@create')->name('series-create');
Route::get('collections/{collection_id}/permissions', 'CollectionPermissionController@index' )->name('collection_permission');
Route::post('collections/{collection_id}/permissions/update', 'CollectionPermissionController@update' )->name('update_permissions');

Route::resource('series', 'SeriesController');
Route::get('series/{series_id}/create_document', 'DocumentController@create')->name('document-create');

Route::resource('documents', 'DocumentController');
Route::get('documents/{document_id}/pdf', 'DocumentController@pdf')->name('document-pdf');
Route::get('documents/{document_id}/digitise', 'DocumentController@createDigitisedText' )->name('create-digitised');
Route::post('documents/{document_id}/digitise/store', 'DocumentController@storeDigitisedText' )->name('store-digitised');

Route::get('documents/{document_id}/tag/add/{user_id}', 'DocumentController@addTags' )->name('add-tags');
Route::get('documents/{document_id}/tag/view/{user_id}', 'DocumentController@viewTags' )->name('view-tags');
Route::get('documents/{document_id}/tag/edit/{user_id}/{tag_id}', 'DocumentController@editTags' )->name('edit-tag');
Route::post('documents/{document_id}/tag/store/{user_id}', 'DocumentController@storeTags' )->name('store-tags');

Route::get('documents/{document_id}/tag/add/{user_id}', 'DocumentController@addTags' )->name('add-tags');
Route::get('documents/{document_id}/tag/view/{user_id}', 'DocumentController@viewTags' )->name('view-tags');
Route::get('documents/{document_id}/tag/edit/{user_id}/{tag_id}', 'DocumentController@editTags' )->name('edit-tag');
Route::post('documents/{document_id}/tag/store/{user_id}', 'DocumentController@storeTags' )->name('store-tags');

Route::resource('tag', 'TagController');
Route::get('documents/{document_id}/tag/view/{user_id}', 'TagController@viewTags' )->name('view-tags');
Route::get('documents/{document_id}/tag/edit/{user_id}/{tag_id}', 'TagController@editTags' )->name('edit-tags');
Route::get('documents/{document_id}/tag/add/{user_id}', 'TagController@addTags' )->name('add-tags');
Route::post('documents/{document_id}/tag/store/{user_id}', 'TagController@storeTags' )->name('store-tags');
Route::get('documents/{document_id}/tag/create', 'TagController@createNewTag' )->name('create-tag');


